#!/bin/sh

# Inicializa o Celery
# cd /usr/src/sentinela || { echo "(EE) Nao encontrei os arquivos do sentinela"; exit 1; }
# celery -A horario_perfeito worker -l info --concurrency 2 &

# Espera o Celery inicializar por completo
sleep 5s

# Inicializa o serviço
cd /usr/src/app || { echo "(EE) Nao encontrei os arquivos do sentinela"; exit 1; }
#sh
python3 manage.py runserver 0.0.0.0:8000 --noreload

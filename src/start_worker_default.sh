#!/bin/bash

source ../venv/bin/activate

# Inicializa o Celery
celery -A sistema_gerenciamento worker -Q celery -l info --concurrency 1 &

from django.db import models

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType

from usuarios.models import Usuario, Sala
from srp.models import SRP, ProdutoSRP


class Produto(models.Model):
    nome = models.CharField(max_length=200)
    data_criacao = models.DateTimeField(auto_now_add=True)
    catmat = models.CharField(max_length=100)
    usuario = models.ForeignKey(Usuario, on_delete=models.PROTECT)


class Descricao(models.Model):
    produto = models.ForeignKey(Produto, on_delete=models.PROTECT)
    usuario = models.ForeignKey(Usuario, on_delete=models.PROTECT)
    texto = models.CharField(max_length=2000)
    anexo = models.FileField()
    data_criacao = models.DateTimeField(auto_now_add=True)


class Empresa(models.Model):
    nome = models.CharField(max_length=200)
    cnpj = models.CharField(max_length=20)
    data_criacao = models.DateTimeField(auto_now_add=True)


class Orcamento(models.Model):
    produto = models.ForeignKey(Produto, on_delete=models.PROTECT)
    usuario = models.ForeignKey(Usuario, on_delete=models.PROTECT)
    valor_unitario = models.FloatField(default=0)
    valido = models.BooleanField(default=True)


class OrcamentoComum(Orcamento):
    empresa = models.ForeignKey(Empresa, on_delete=models.PROTECT)
    arquivo = models.FileField()
    data = models.DateTimeField(auto_now_add=True, blank=True)


class OrcamentoComprasNet(Orcamento):
    compras_net = models.BooleanField(default=False)
    url = models.URLField()
    pregao = models.CharField(max_length=100)


class OrcamentoSRP(Orcamento):
    srp = models.ForeignKey(SRP, on_delete=models.PROTECT)
    produto_srp = models.ForeignKey(ProdutoSRP, on_delete=models.PROTECT)


class Pedido(models.Model):
    ESTADO_PEDIDO = '1'
    ESTADO_LIBERADO_DAE = '2'
    ESTADO_LICITANDO = '3'
    ESTADO_ANALISE_TECNICA = '4'
    ESTADO_AGUARDANDO_RECEBIMENTO = '5'
    ESTADO_AGUARDANDO_ATESTE = '6'
    ESTADO_AGUARDANDO_PAGAMENTO = '7'
    ESTADO_AGUARDANDO_NUMERO_PATRIMONIO = '8'
    ESTADO_FINALIZADO = '9'
    ESTADOS = (
        (ESTADO_PEDIDO, 'Pedido'),
        (ESTADO_LIBERADO_DAE, 'Liberado pelo DAE'),
        (ESTADO_LICITANDO, 'Licitando'),
        (ESTADO_ANALISE_TECNICA, 'Em análise pela equipe ténica'),
        (ESTADO_AGUARDANDO_RECEBIMENTO, 'Aguardando recebimento'),
        (ESTADO_AGUARDANDO_ATESTE, 'Aguardando ateste'),
        (ESTADO_AGUARDANDO_PAGAMENTO, 'Aguardando pagamento'),
        (ESTADO_AGUARDANDO_NUMERO_PATRIMONIO, 'Aguardado número de patrimônio'),
        (ESTADO_FINALIZADO, 'Finalizado'),
    )

    usuario = models.ForeignKey(Usuario, on_delete=models.PROTECT)
    produto = models.ForeignKey(Produto, on_delete=models.PROTECT)

    quantidade = models.IntegerField(default=1)
    unidade_fornecimento = models.CharField(max_length=10)

    destino = models.ForeignKey(Sala, on_delete=models.PROTECT)
    data_criacao = models.DateTimeField(auto_now_add=True)
    motivo = models.CharField(max_length=1000, blank=False)
    estado = models.CharField(choices=ESTADOS, max_length=5)
    valor = models.FloatField(default=0)

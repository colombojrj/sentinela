from django.urls import path
from django.contrib.auth.decorators import login_required, permission_required

from . import views

app_name = 'almoxarifado'

urlpatterns = [
    path(
        '',
        views.Index.as_view(),
        name='index'
    ),
    path(
        'equipamentos/',
        views.IndexEquipamentos.as_view(),
        name='index_equipamentos'
    ),
    path(
        'equipamentos/criado/fabricante/<int:id_fabricante>/',
        views.IndexEquipamentos.as_view(),
        name='index_equipamentos'
    ),
    path(
        'equipamentos/criado/modelo/<int:id_modelo>/',
        views.IndexEquipamentos.as_view(),
        name='index_equipamentos'
    ),
    path(
        'equipamentos/criar/fabricante',
        views.CriarFabricante.as_view(),
        name='criar_fabricante'
    ),
    path(
        'equipamentos/ver/fabricantes',
        views.VerFabricantes.as_view(),
        name='ver_fabricantes'
    ),
    path(
        'equipamentos/editar/fabricante/<int:id_fabricante>',
        views.EditarFabricante.as_view(),
        name='editar_fabricante'
    ),
    path(
        'equipamentos/criar/modelo',
        views.CriarModelo.as_view(),
        name='criar_modelo'
    ),
    path(
        'equipamentos/ver/modelos',
        views.VerModelos.as_view(),
        name='ver_modelos'
    ),
    path(
        'equipamentos/editar/modelo/<int:id_modelo>',
        views.EditarModelo.as_view(),
        name='editar_modelo'
    ),
    path(
        'criar/tipo-de-item/',
        views.CriarTipoItem.as_view(),
        name='criar_tipo_item'
    ),
]


from django.contrib import admin
from .models import *


class AdminSala(admin.ModelAdmin):
    list_display = ('sigla', 'nome', )


class AdminFabricante(admin.ModelAdmin):
    list_display = ('nome', )


class AdminModelo(admin.ModelAdmin):
    list_display = ('fabricante', 'nome', )


class AdminCategoria(admin.ModelAdmin):
    list_display = ('nome', )


class AdminItem(admin.ModelAdmin):
    list_display = ('tipo', 'categoria', 'modelo',)


admin.site.register(Sala, AdminSala)
admin.site.register(Fabricante, AdminFabricante)
admin.site.register(Modelo, AdminModelo)
admin.site.register(Categoria, AdminCategoria)
admin.site.register(Item, AdminItem)

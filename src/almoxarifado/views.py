from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.forms import TimeInput
from django.http import JsonResponse
from django.views import View
from django.views.generic.base import TemplateView
from django.db.models import Count
from django.core.serializers import serialize

from usuarios.views import SentinelaViewBasica

from .models import *
from .forms import *


class AlmoxarifadoViewBasica(SentinelaViewBasica):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.texto_aviso_verde = None
        self.texto_aviso_vermelho = None

        self.usuario = None
        self.instituicao = None
        self.contexto = dict()

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)

        self.usuario = self.selecionar_usuario(request)
        self.contexto['usuario'] = self.usuario


class Index(AlmoxarifadoViewBasica):
    def get(self, request, *args, **kwargs):
        return render(request, 'almoxarifado/index.html', self.contexto)

    def post(self, request, *args, **kwargs):
        return render(request, 'almoxarifado/index.html', self.contexto)


class IndexEquipamentos(AlmoxarifadoViewBasica):
    def get(self, request, id_fabricante=None, id_modelo=None, *args, **kwargs):
        fabricantes = Fabricante.objects.all()
        self.contexto['fabricantes'] = fabricantes

        modelos = Modelo.objects.all()
        self.contexto['modelos'] = modelos

        equipamentos = Equipamento.objects.filter()
        self.contexto['equipamentos'] = equipamentos

        if id_fabricante is not None:
            self.contexto['mostrar_barra_aviso_verde'] = True
            self.contexto['fabricante_criado_com_sucesso'] = True
            self.contexto['novo_fabricante'] = Fabricante.objects.get(id=id_fabricante)

        elif id_modelo is not None:
            self.contexto['mostrar_barra_aviso_verde'] = True
            self.contexto['modelo_criado_com_sucesso'] = True
            self.contexto['novo_modelo'] = Modelo.objects.get(id=id_modelo)
        return render(request, 'almoxarifado/index_equipamentos.html', self.contexto)

    def post(self, request, *args, **kwargs):
        return self.get(self, request, *args, **kwargs)


class CriarFabricante(AlmoxarifadoViewBasica):
    def get(self, request, *args, **kwargs):
        self.contexto['form'] = FormNovoFabricante()
        return render(request, 'almoxarifado/criar_fabricante.html', self.contexto)

    def post(self, request, *args, **kwargs):
        form = FormNovoFabricante(request.POST)
        if form.is_valid():
            novo_fabricante = form.save()
            return redirect('almoxarifado:index_equipamentos', id_fabricante=novo_fabricante.id)
        else:
            return render(request, 'almoxarifado/criar_fabricante.html', self.contexto)


class VerFabricantes(AlmoxarifadoViewBasica):
    def get(self, request, *args, **kwargs):
        self.contexto['fabricantes'] = Fabricante.objects.all()
        return render(request, 'almoxarifado/ver_fabricantes.html', self.contexto)

    def post(self, request, *args, **kwargs):
        return render(request, 'almoxarifado/ver_fabricantes.html', self.contexto)


class EditarFabricante(AlmoxarifadoViewBasica):
    def comum(self, id_fabricante):
        self.fabricante = Fabricante.objects.get(id=id_fabricante)
        self.contexto['fabricante'] = self.fabricante

    def get(self, request, id_fabricante, *args, **kwargs):
        self.comum(id_fabricante)
        self.contexto['form'] = FormEditarFabricante(instance=self.fabricante)
        return render(request, 'almoxarifado/editar_fabricante.html', self.contexto)

    def post(self, request, id_fabricante, *args, **kwargs):
        self.comum(id_fabricante)
        form = FormEditarFabricante(
            request.POST,
            instance=self.fabricante
        )
        form.save()
        self.contexto['form'] = form

        if form.is_valid():
            self.contexto['mostrar_barra_aviso_verde'] = True
            self.contexto['alteracoes_sucesso'] = True
        return render(request, 'almoxarifado/editar_fabricante.html', self.contexto)


class CriarModelo(AlmoxarifadoViewBasica):
    def get(self, request, *args, **kwargs):
        self.contexto['form'] = FormNovoModelo()
        return render(request, 'almoxarifado/criar_modelo.html', self.contexto)

    def post(self, request, *args, **kwargs):
        form = FormNovoModelo(request.POST, request.FILES)
        if form.is_valid():
            novo_modelo = form.save()
            return redirect('almoxarifado:index_equipamentos', id_modelo=novo_modelo.id)
        else:
            return render(request, 'almoxarifado/criar_modelo.html', self.contexto)


class VerModelos(AlmoxarifadoViewBasica):
    def get(self, request, *args, **kwargs):
        self.contexto['modelos'] = Modelo.objects.all()
        return render(request, 'almoxarifado/ver_modelos.html', self.contexto)

    def post(self, request, *args, **kwargs):
        return render(request, 'almoxarifado/ver_modelos.html', self.contexto)


class EditarModelo(AlmoxarifadoViewBasica):
    def comum(self, id_modelo):
        self.modelo = Modelo.objects.get(id=id_modelo)
        self.contexto['modelo'] = self.modelo

    def get(self, request, id_modelo, *args, **kwargs):
        self.comum(id_modelo)
        self.contexto['form'] = FormEditarModelo(instance=self.modelo)
        return render(request, 'almoxarifado/editar_modelo.html', self.contexto)

    def post(self, request, id_modelo, *args, **kwargs):
        self.comum(id_modelo)
        form = FormEditarModelo(
            request.POST,
            instance=self.modelo
        )
        form.save()
        self.contexto['form'] = form

        if form.is_valid():
            self.contexto['mostrar_barra_aviso_verde'] = True
            self.contexto['alteracoes_sucesso'] = True
        return render(request, 'almoxarifado/editar_modelo.html', self.contexto)


class CriarTipoItem(AlmoxarifadoViewBasica):
    def get(self, request, *args, **kwargs):
        form = FormCriarTipoMaterial()
        self.contexto['form'] = form
        return render(request, 'almoxarifado/criar_tipo_item.html', self.contexto)

    def post(self, request, *args, **kwargs):
        form = FormCriarTipoMaterial(request.POST)
        if form.is_valid():
            form.save()
            self.contexto['mostrar_barra_aviso_verde'] = True
            self.contexto['alteracoes_sucesso'] = True
        self.contexto['form'] = form
        redirect('almoxarifado:index_equipamentos')


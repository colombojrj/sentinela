# Generated by Django 2.2.9 on 2020-01-07 23:32

import ckeditor_uploader.fields
from django.db import migrations, models
import django.db.models.deletion
import educacao.models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('usuarios', '0002_remove_usuario_area'),
    ]

    operations = [
        migrations.CreateModel(
            name='Alternativa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('codigo', models.CharField(default='a', max_length=10)),
                ('enunciado', ckeditor_uploader.fields.RichTextUploadingField()),
                ('data_criacao', models.DateTimeField(auto_now_add=True)),
                ('resposta_correta', models.CharField(blank=True, choices=[('V', 'Verdadeiro'), ('F', 'Falso')], max_length=2)),
                ('feedback', ckeditor_uploader.fields.RichTextUploadingField(blank=True)),
                ('nota_maxima', models.FloatField(default=1)),
            ],
        ),
        migrations.CreateModel(
            name='Avaliacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo', models.CharField(choices=[('P', 'Prova'), ('T', 'Trabalho'), ('R', 'Relatório'), ('S', 'Seminário'), ('L', 'Lista de exercícios')], max_length=2)),
                ('titulo', models.CharField(max_length=100)),
                ('descricao', ckeditor_uploader.fields.RichTextUploadingField(blank=True)),
                ('individual', models.BooleanField(default=True)),
                ('numero_maximo_integrantes_grupo', models.IntegerField(default=3)),
                ('numero', models.PositiveSmallIntegerField(default=1)),
                ('numero_questoes', models.SmallIntegerField(default=1)),
                ('nota_maxima', models.FloatField(default=10)),
                ('data_criacao', models.DateTimeField(auto_now_add=True)),
                ('visibilidade', models.CharField(choices=[('O', 'Oculto'), ('V', 'Visível'), ('A', 'Visível após a data')], default='O', max_length=2)),
                ('data_para_ficar_visivel', models.DateTimeField(blank=True, null=True)),
                ('correcao_automatica', models.BooleanField(default=False)),
                ('correcao_apos_data_limite', models.BooleanField(default=True)),
                ('descontar_um_ponto_a_cada_n_erradas', models.PositiveSmallIntegerField(default=0)),
                ('penalidade_por_dia_atraso', models.PositiveIntegerField(default=1)),
                ('data_limite_entrega', models.DateTimeField(blank=True, null=True)),
                ('aceita_envios_apenas_antes_data_limite', models.BooleanField(default=False)),
                ('numero_maximo_tentativas', models.PositiveSmallIntegerField(default=10)),
                ('permitir_resubmissao', models.BooleanField(default=True)),
            ],
            options={
                'verbose_name_plural': 'Avaliações',
                'ordering': ['disciplina', 'numero'],
            },
        ),
        migrations.CreateModel(
            name='Disciplina',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=100)),
                ('sigla', models.CharField(max_length=10)),
                ('numero_aulas', models.PositiveSmallIntegerField(default=19)),
                ('data_criacao', models.DateTimeField(auto_now_add=True)),
                ('turma', models.CharField(max_length=10)),
                ('objetivo', ckeditor_uploader.fields.RichTextUploadingField()),
                ('icone', models.FileField(upload_to=educacao.models.Caminho('upload/disciplinas'))),
                ('visivel', models.BooleanField(default=True)),
                ('criterios_avaliacao', ckeditor_uploader.fields.RichTextUploadingField()),
                ('bibliografia', ckeditor_uploader.fields.RichTextUploadingField()),
                ('senha', models.CharField(blank=True, max_length=20)),
                ('aberto_para_inscricoes', models.BooleanField(default=False)),
                ('numero_provas', models.PositiveSmallIntegerField(default=0)),
                ('numero_relatorios', models.PositiveSmallIntegerField(default=0)),
                ('numero_trabalhos', models.PositiveSmallIntegerField(default=0)),
                ('numero_listas', models.PositiveSmallIntegerField(default=0)),
                ('numero_matriculas', models.PositiveSmallIntegerField(default=0)),
                ('numero_seminarios', models.PositiveSmallIntegerField(default=0)),
                ('inscritos', models.ManyToManyField(blank=True, related_name='disciplina_inscritos', to='usuarios.Usuario')),
                ('professores', models.ManyToManyField(blank=True, to='usuarios.Usuario')),
            ],
            options={
                'verbose_name_plural': 'Disciplinas',
                'ordering': ['-turma'],
            },
        ),
        migrations.CreateModel(
            name='GrupoAvaliacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_criacao', models.DateTimeField(auto_now_add=True)),
                ('avaliacao', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='educacao.Avaliacao')),
            ],
        ),
        migrations.CreateModel(
            name='Questao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('tipo', models.CharField(choices=[('ME', 'Múltipla escolha'), ('VF', 'Verdadeiro ou falso'), ('D', 'Dissertativa')], max_length=2)),
                ('enunciado', ckeditor_uploader.fields.RichTextUploadingField(blank=True)),
                ('data_criacao', models.DateTimeField(auto_now_add=True)),
                ('numero_respostas', models.PositiveIntegerField(default=0)),
                ('nota_media', models.FloatField(default=0)),
                ('desvio_padrao_nota', models.FloatField(default=0)),
                ('numero_alternativas', models.PositiveIntegerField(default=0)),
                ('nota_maxima', models.FloatField(default=1)),
            ],
            options={
                'verbose_name_plural': 'Questões',
            },
        ),
        migrations.CreateModel(
            name='QuestaoAvaliacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numero', models.PositiveSmallIntegerField(default=1)),
                ('avaliacao', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='educacao.Avaliacao')),
                ('questao', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='educacao.Questao')),
            ],
            options={
                'verbose_name_plural': 'Questões da avaliacao',
                'ordering': ['numero'],
            },
        ),
        migrations.CreateModel(
            name='Resposta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_criacao', models.DateTimeField(auto_now_add=True)),
                ('data_submissao', models.DateTimeField(blank=True, null=True)),
                ('feedback', ckeditor_uploader.fields.RichTextUploadingField(blank=True)),
                ('nota', models.FloatField(default=0)),
                ('corrigido', models.BooleanField(default=False)),
                ('data_correcao', models.DateTimeField(blank=True, null=True)),
                ('respondida', models.BooleanField(default=False)),
                ('numero_tentativas', models.PositiveSmallIntegerField(default=0)),
                ('resposta', models.CharField(blank=True, max_length=2)),
                ('conteudo', ckeditor_uploader.fields.RichTextUploadingField(blank=True)),
                ('numero_anexos', models.PositiveSmallIntegerField(default=0)),
                ('alternativa', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, related_name='resposta_alternativa', to='educacao.Alternativa')),
                ('aluno', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='usuarios.Usuario')),
                ('avaliacao', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='educacao.Avaliacao')),
                ('disciplina', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='educacao.Disciplina')),
                ('questao', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='resposta_questao', to='educacao.Questao')),
                ('questao_avaliacao', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='resposta_questaoavaliacao', to='educacao.QuestaoAvaliacao')),
            ],
            options={
                'verbose_name_plural': 'Respostas',
                'ordering': ['disciplina', 'questao_avaliacao', 'aluno'],
            },
        ),
        migrations.CreateModel(
            name='NotaAlunoAvaliacao',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_criacao', models.DateField(auto_now_add=True)),
                ('nota', models.FloatField(default=0)),
                ('aluno', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='usuarios.Usuario')),
                ('avaliacao', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='educacao.Avaliacao')),
            ],
        ),
        migrations.CreateModel(
            name='Matricula',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_criacao', models.DateTimeField(auto_now_add=True)),
                ('aluno', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='usuarios.Usuario')),
                ('disciplina', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='educacao.Disciplina')),
            ],
        ),
        migrations.CreateModel(
            name='IntegranteGrupo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_criacao', models.DateTimeField(auto_now_add=True)),
                ('aluno', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='usuarios.Usuario')),
                ('grupo', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='educacao.GrupoAvaliacao')),
            ],
        ),
        migrations.AddField(
            model_name='avaliacao',
            name='disciplina',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='educacao.Disciplina'),
        ),
        migrations.CreateModel(
            name='Aula',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('texto', ckeditor_uploader.fields.RichTextUploadingField()),
                ('data_criacao', models.DateTimeField(auto_now_add=True)),
                ('data_da_aula', models.DateTimeField()),
                ('numero', models.PositiveSmallIntegerField(default=1)),
                ('visivel', models.BooleanField(default=False)),
                ('disciplina', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='educacao.Disciplina')),
            ],
            options={
                'verbose_name_plural': 'Aulas',
                'ordering': ['numero'],
            },
        ),
        migrations.CreateModel(
            name='ArtigoAula',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data_criacao', models.DateField(auto_now_add=True)),
                ('titulo', models.CharField(max_length=1000)),
                ('conteudo', ckeditor_uploader.fields.RichTextUploadingField()),
                ('aula', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='educacao.Aula')),
            ],
        ),
        migrations.CreateModel(
            name='AnexoResposta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome_arquivo', models.CharField(max_length=100)),
                ('tipo', models.CharField(choices=[('CF', 'Código fonte'), ('D', 'Documento'), ('I', 'Imagem'), ('O', 'Outros')], default='D', max_length=2)),
                ('arquivo', models.FileField(upload_to=educacao.models.Caminho('upload/anexo_resposta'))),
                ('data_criacao', models.DateField(auto_now_add=True)),
                ('resposta', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='educacao.Resposta')),
            ],
        ),
        migrations.CreateModel(
            name='AnexoAula',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome_arquivo', models.CharField(max_length=100)),
                ('tipo', models.CharField(choices=[('CF', 'Código fonte'), ('D', 'Documento'), ('I', 'Imagem'), ('O', 'Outros')], default='D', max_length=2)),
                ('arquivo', models.FileField(upload_to=educacao.models.Caminho('upload/anexo_aula'))),
                ('descricao', ckeditor_uploader.fields.RichTextUploadingField()),
                ('data_criacao', models.DateField(auto_now_add=True)),
                ('aula', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='educacao.Aula')),
            ],
        ),
        migrations.AddField(
            model_name='alternativa',
            name='questao',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='educacao.Questao'),
        ),
    ]

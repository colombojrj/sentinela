from django.apps import AppConfig


class EducacaoConfig(AppConfig):
    name = 'educacao'

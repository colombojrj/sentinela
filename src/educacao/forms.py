from django import forms
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User
from django.forms import formset_factory

from .models import *
from usuarios.models import Usuario

try:
    from src.lab_remoto.models import ParametroInformado, ExecucaoEnsaio
except:
    from lab_remoto.models import ParametroInformado, ExecucaoEnsaio

class FormCriarDisciplina(forms.ModelForm):
    # Considerações sobre a criação de disciplinas
    # - as atividades avaliativas são criadas depois
    # - os criterios de avaliação serão preenchidos depois
    # - a bibliografia será inserida depois
    # - quando a disciplina é criada, ela não está aceitando matriculas
    class Meta:
        model = Disciplina
        exclude = (
            'data_criacao',
            'objetivo',
            'professor',
            'criterios_avaliacao',
            'bibliografia',
            'numero_provas',
            'numero_relatorios',
            'numero_trabalhos',
            'numero_listas',
            'numero_matriculas',
            'numero_seminarios',
            'aberto_para_inscricoes',
            'inscritos',
            'professores',
        )


class FormInscreverDisciplina(forms.Form):
    senha = forms.CharField(
        label='Senha',
        max_length=50
    )


class FormEditarDisciplina(forms.ModelForm):
    # Considerações sobre a criação de disciplinas
    # - a quantidade de atividades é gerenciada automaticamente
    class Meta:
        model = Disciplina
        exclude = (
            'data_criacao',
            'numero_aulas',
            'numero_provas',
            'numero_relatorios',
            'numero_trabalhos',
            'numero_listas',
            'numero_matriculas',
            'numero_seminarios',
            'inscritos',
        )

    def __init__(self, *args, **kwargs):
        super(FormEditarDisciplina, self).__init__(*args, **kwargs)
        self.fields['professores'].queryset = Usuario.objects.filter(tipo=Usuario.TIPO_DOCENTE)


class FormEditarAula(forms.ModelForm):
    class Meta:
        model = Aula
        exclude = ('data_criacao', )


class FormCriarArtigo(forms.ModelForm):
    class Meta:
        model = ArtigoAula
        exclude = ('aula', 'data_criacao',)


class FormEditarQuestao(forms.ModelForm):
    class Meta:
        model = Questao
        fields = ('nota_maxima', 'tipo', 'enunciado', )


class FormEditarAlternativaMultiplaEscolha(forms.ModelForm):
    class Meta:
        model = Alternativa
        fields = ('codigo', 'nota_maxima', 'enunciado', 'resposta_correta', 'feedback', )


FormsetEditarAlternativasMultiplaEscolha = forms.modelformset_factory(
    Alternativa,
    form=FormEditarAlternativaMultiplaEscolha,
    extra=0
)


class FormEditarAlternativaDissertativa(forms.ModelForm):
    class Meta:
        model = Alternativa
        fields = ('questao', 'codigo', 'enunciado', 'feedback', )


FormsetEditarAlternativasDissertativa = forms.modelformset_factory(
    Alternativa,
    form=FormEditarAlternativaDissertativa,
    extra=0
)


class FormEditarListaExercicios(forms.ModelForm):
    # Considerações sobre listas de exercícios
    # - uma avaliação não pode ser convertida em outra coisa (exemplo: prova -> trabalho)
    # - são sempre individuais

    class Meta:
        model = Avaliacao
        exclude = ('tipo', 'individual', 'numero_maximo_integrantes_grupo',)


class FormEditarRelatorio(forms.ModelForm):
    class Meta:
        model = Avaliacao
        exclude = (
            'tipo',
            'numero',
            'numero_questoes',
            'nota_maxima',
            'disciplina',
            'data_criacao',
            'correcao_automatica',
            'descontar_um_ponto_a_cada_n_erradas',
            'penalidade_por_dia_atraso',
            'individual',
        )


class FormFilmarExperimento(forms.ModelForm):
    class Meta:
        model = ExecucaoEnsaio
        fields = (
            'filmar',
        )

        labels = {
            'filmar': _('Sim eu quero filmar o experimento'),
        }


class FormEditarParametrosInformados(forms.ModelForm):
    class Meta:
        model = ParametroInformado
        fields = (
            'valor_informado',
        )


FormsetEditarParametrosInformados = forms.modelformset_factory(
    ParametroInformado,
    form=FormEditarParametrosInformados,
    extra=0
)


class FormCriarResposta(forms.ModelForm):
    class Meta:
        model = Resposta
        fields = ('resposta', 'questao', 'alternativa', )


FormsetCriarRespostaAlternativa = forms.modelformset_factory(
    Resposta,
    form=FormCriarResposta,
    extra=0
)


class FormEditarResposta(forms.ModelForm):
    class Meta:
        model = Resposta
        fields = ('resposta', )


FormsetEditarRespostaAlternativa = forms.modelformset_factory(
    Resposta,
    form=FormEditarResposta,
    extra=0
)


class FormEditarRespostaDissertativa(forms.ModelForm):
    class Meta:
        model = Resposta
        fields = ('conteudo',)


class FormEditarRespostaMultiplaEscolha(forms.ModelForm):
    class Meta:
        model = Resposta
        fields = ('resposta',)

        widgets = {
            'resposta': forms.RadioSelect()
        }

    def __init__(self, *args, **kwargs):
        self.opcoes = kwargs.pop('opcoes')
        super(FormEditarRespostaMultiplaEscolha, self).__init__(*args, **kwargs)
        self.fields['resposta'].choices = self.opcoes


FabricaEditarRespostaMultiplaEscolha = forms.modelformset_factory(
    Resposta,
    form=FormEditarRespostaMultiplaEscolha,
    extra=0
)


class FormsetEditarRespostaMultiplaEscolha(FabricaEditarRespostaMultiplaEscolha):
    def __init__(self, *args, **kwargs):
        #  create a user attribute and take it out from kwargs
        # so it doesn't messes up with the other formset kwargs
        self.opcoes = kwargs.pop('opcoes')
        super(FormsetEditarRespostaMultiplaEscolha, self).__init__(*args, **kwargs)
        for form in self.forms:
            form.empty_permitted = False

    def _construct_form(self, *args, **kwargs):
        # inject user in each form on the formset
        kwargs['opcoes'] = self.opcoes
        return super(FormsetEditarRespostaMultiplaEscolha, self)._construct_form(*args, **kwargs)


class FormAdicionarAnexoResposta(forms.ModelForm):
    class Meta:
        model = AnexoResposta
        fields = ('resposta', 'nome_arquivo', 'tipo', 'arquivo',)

    def __init__(self, *args, **kwargs):
        self.ids_respostas = kwargs.pop('ids_respostas')
        super(FormAdicionarAnexoResposta, self).__init__(*args, **kwargs)
        self.fields['resposta'].queryset = Resposta.objects.filter(id__in=self.ids_respostas)


class FormAdicionarAnexoRelatorio(forms.ModelForm):
    class Meta:
        model = AnexoRelatorio
        fields = ('nome_arquivo', 'tipo', 'arquivo',)


class FormCorrigirResposta(forms.ModelForm):
    class Meta:
        model = Resposta
        fields = ('nota', 'feedback', )


FabricaCorrigirResposta = forms.modelformset_factory(
    Resposta,
    form=FormCorrigirResposta,
    extra=0
)


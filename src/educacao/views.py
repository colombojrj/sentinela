from django.shortcuts import render, redirect
from django.db.models import Count, Sum, Avg, StdDev
from django.conf import settings

import datetime
import pytz

from .models import *
from .forms import *
from lab_remoto.forms import FormEditarTagExecucao
from usuarios.views import SentinelaViewBasica
from .tasks import executar_experimento, executar_experimento_assincrono


# Laboratório remoto
try:
    from src.lab_remoto.models import Ensaio, KitDidatico, ExecucaoEnsaio, ParametroInformado, ParametroParaSerInformado
    from src.lab_remoto.models import ParametroDoKit
    from src.drivers.port import executar_codigo
except:
    from lab_remoto.models import Ensaio, KitDidatico, ExecucaoEnsaio, ParametroInformado, ParametroParaSerInformado
    from lab_remoto.models import ParametroDoKit
    from drivers.port import executar_codigo


# Biblioteca para carregar arquivos matlab
from scipy.io import loadmat


# Biblioteca para geração de gráficos
from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.palettes import Viridis256, Category20c
from bokeh.transform import cumsum


def get_next_or_prev(models, item, direction):
    """
    Returns the next or previous item of
    a query-set for 'item'.

    'models' is a query-set containing all
    items of which 'item' is a part of.

    direction is 'next' or 'prev'
    """

    if direction == 'next' and models.last() == item:
        return None
    elif direction == 'prev' and models.first() == item:
        return None
    else:
        getit = False
        if direction == 'prev':
            models = models.reverse()
        for m in models:
            if getit:
                return m
            if item == m:
                getit = True
        if getit:
            # This would happen when the last
            # item made getit True
            return models[0]
        return None


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')

    bad_list = ('', None,)
    if ip in bad_list:
        ip = '0.0.0.0'

    return ip


class EducacaoViewBasica(SentinelaViewBasica):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.texto_aviso_verde = None
        self.texto_aviso_vermelho = None

        # self.usuario = None
        # self.instituicao = None
        self.contexto = dict()


class Index(EducacaoViewBasica):
    def get(self, request, id_disciplina=None, *args, **kwargs):
        disciplinas = Disciplina.objects.all().order_by('-data_criacao')
        disciplinas = disciplinas.annotate(matriculas=Count('matricula'))
        self.contexto['disciplinas'] = disciplinas

        if id_disciplina is not None:
            self.contexto['mostrar_barra_aviso_verde'] = True
            self.contexto['alteracoes_sucesso'] = True
        return render(request, 'educacao/index.html', self.contexto)

    def post(self, request, *args, **kwargs):
        return render(request, 'educacao/index.html', self.contexto)


class CriarDisciplina(EducacaoViewBasica):
    def get(self, request, *args, **kwargs):
        dados = {
            'turma': datetime.datetime.now().year
        }
        form = FormCriarDisciplina(initial=dados)
        self.contexto['form'] = form
        return render(request, 'educacao/criar_disciplina.html', self.contexto)

    def post(self, request, *args, **kwargs):
        form = FormCriarDisciplina(request.POST, request.FILES)

        if form.is_valid():
            nova_disciplina = form.save(commit=False)
            nova_disciplina.objetivo = 'Em breve!'
            nova_disciplina.bibliografia = 'Em breve!'
            nova_disciplina.criterios_avaliacao = 'Em breve!'
            nova_disciplina.save()
            nova_disciplina.professores.add(self.usuario)

            for n in range(1, (nova_disciplina.numero_aulas+1)):
                nova_aula = Aula(
                    disciplina=nova_disciplina,
                    descricao='Em breve!',
                    data_da_aula=datetime.datetime.now(),
                    numero=n
                )
                nova_aula.save()
            return redirect('educacao:index', id_disciplina=nova_disciplina.id)
        else:
            form = FormCriarDisciplina()
            self.contexto['form'] = form
            return render(request, 'educacao/criar_disciplina.html', self.contexto)


class EditarDisciplina(EducacaoViewBasica):
    def comum(self, id_disciplina):
        self.disciplina = Disciplina.objects.get(id=id_disciplina)
        self.contexto['disciplina'] = self.disciplina

    def get(self, request, id_disciplina, *args, **kwargs):
        self.comum(id_disciplina)
        form = FormEditarDisciplina(instance=self.disciplina)
        self.contexto['form'] = form
        return render(request, 'educacao/editar_disciplina.html', self.contexto)

    def post(self, request, id_disciplina, *args, **kwargs):
        self.comum(id_disciplina)
        form = FormEditarDisciplina(
            request.POST,
            request.FILES,
            instance=self.disciplina
        )

        if form.is_valid():
            form.save()
            self.contexto['form'] = form
            self.contexto['mostrar_barra_aviso_verde'] = True
            self.contexto['alteracoes_sucesso'] = True
        return render(request, 'educacao/editar_disciplina.html', self.contexto)


class VerDisciplina(EducacaoViewBasica):
    def get(self, request, id_disciplina, *args, **kwargs):
        disciplina = Disciplina.objects.get(id=id_disciplina)
        self.contexto['disciplina'] = disciplina

        aulas = Aula.objects.filter(disciplina=disciplina).order_by('numero')
        self.contexto['aulas'] = aulas

        # if self.usuario in disciplina.matri
        listas = Avaliacao.objects.filter(
            tipo=Avaliacao.AVALIACAO_LISTA_EXERCICIOS,
            disciplina=disciplina
        )
        self.contexto['listas'] = listas

        trabalhos = Avaliacao.objects.filter(
            tipo=Avaliacao.AVALIACAO_TRABALHO,
            disciplina=disciplina
        )
        self.contexto['trabalhos'] = trabalhos

        relatorios = Avaliacao.objects.filter(
            tipo=Avaliacao.AVALIACAO_RELATORIO,
            disciplina=disciplina
        )
        self.contexto['relatorios'] = relatorios

        provas = Avaliacao.objects.filter(
            tipo=Avaliacao.AVALIACAO_PROVA,
            disciplina=disciplina
        )
        self.contexto['provas'] = provas

        seminarios = Avaliacao.objects.filter(
            tipo=Avaliacao.AVALIACAO_SEMINARIO,
            disciplina=disciplina
        )
        self.contexto['seminarios'] = seminarios

        return render(request, 'educacao/ver_disciplina.html', self.contexto)


class InscreverNaDisciplina(EducacaoViewBasica):
    def get(self, request, id_disciplina, *args, **kwargs):
        disciplina = Disciplina.objects.get(id=id_disciplina)
        self.contexto['disciplina'] = disciplina

        if len(disciplina.senha) is 0:
            disciplina.inscritos.add(self.usuario)
            return redirect('educacao:ver_disciplina', disciplina.id)

        else:
            form = FormInscreverDisciplina()
            self.contexto['form'] = form
            return render(request, 'educacao/inscrever_na_disciplina.html', self.contexto)

    def post(self, request, id_disciplina, *args, **kwargs):
        disciplina = Disciplina.objects.get(id=id_disciplina)
        self.contexto['disciplina'] = disciplina

        form = FormInscreverDisciplina(request.POST)
        if form.data['senha'] == disciplina.senha:
            disciplina.inscritos.add(self.usuario)
            return redirect('educacao:ver_disciplina', disciplina.id)
        else:
            self.contexto['aviso_barra_vermelha'] = 'Senha incorreta'
            self.contexto['mostrar_barra_aviso_vermelha'] = True
            self.contexto['form'] = form
            return render(request, 'educacao/inscrever_na_disciplina.html', self.contexto)


class VerAula(EducacaoViewBasica):
    def get(self, request, id_aula, *args, **kwargs):
        aula = Aula.objects.get(id=id_aula)
        self.contexto['aula'] = aula

        anexos = AnexoAula.objects.filter(aula=aula)
        self.contexto['anexos'] = anexos

        artigos = ArtigoAula.objects.filter(aula=aula)
        self.contexto['artigos'] = artigos

        return render(request, 'educacao/ver_aula.html', self.contexto)


class CriarArtigo(EducacaoViewBasica):
    def get(self, request, id_aula):
        aula = Aula.objects.get(id=id_aula)
        novo_artigo = ArtigoAula(
            aula=aula,
            conteudo='Por favor preencha com o conteúdo.',
            titulo='Por favor preencha com o título do artigo'
        )
        novo_artigo.save()
        return redirect('educacao:editar_artigo', novo_artigo.id)


class EditarArtigo(EducacaoViewBasica):
    def get(self, request, id_artigo):
        artigo = ArtigoAula.objects.get(id=id_artigo)
        form = FormCriarArtigo(instance=artigo)
        self.contexto['form'] = form
        return render(request, 'educacao/editar_artigo.html', self.contexto)

    def post(self, request, id_artigo):
        artigo = ArtigoAula.objects.get(id=id_artigo)
        form = FormCriarArtigo(
            request.POST,
            instance=artigo
        )

        if form.is_valid():
            form.save()

        self.contexto['form'] = form
        return render(request, 'educacao/editar_artigo.html', self.contexto)


class VerArtigo(EducacaoViewBasica):
    def get(self, request, id_artigo):
        artigo = ArtigoAula.objects.get(id=id_artigo)
        self.contexto['artigo'] = artigo

        return render(request, 'educacao/ver_artigo.html', self.contexto)


class EditarAula(EducacaoViewBasica):
    def comum(self, id_aula):
        self.aula = Aula.objects.get(id=id_aula)
        self.contexto['aula'] = self.aula

    def get(self, request, id_aula, *args, **kwargs):
        self.comum(id_aula)
        form = FormEditarAula(instance=self.aula)
        self.contexto['form'] = form
        return render(request, 'educacao/editar_aula.html', self.contexto)

    def post(self, request, id_aula, *args, **kwargs):
        self.comum(id_aula)
        form = FormEditarAula(
            request.POST,
            request.FILES,
            instance=self.aula
        )
        form.save()
        self.contexto['form'] = form

        if form.is_valid():
            self.contexto['mostrar_barra_aviso_verde'] = True
            self.contexto['alteracoes_sucesso'] = True
        return render(request, 'educacao/editar_aula.html', self.contexto)


class CriarListaExercicios(EducacaoViewBasica):
    def get(self, request, id_disciplina, *args, **kwargs):
        self.disciplina = Disciplina.objects.get(id=id_disciplina)
        self.listas = Avaliacao.objects.filter(
            disciplina=self.disciplina,
            tipo=Avaliacao.AVALIACAO_LISTA_EXERCICIOS
        )

        numero_lista = self.listas.count() + 1
        nova_lista = Avaliacao(
            tipo=Avaliacao.AVALIACAO_LISTA_EXERCICIOS,
            individual=True,
            numero_maximo_integrantes_grupo=1,
            titulo='Lista de exercícios %d' % numero_lista,
            numero=numero_lista,
            disciplina=self.disciplina
        )
        nova_lista.save()
        self.contexto['lista_exercicios'] = nova_lista

        # Aumenta o número de listas de exercícios da disciplina
        self.disciplina.numero_listas = self.disciplina.numero_listas + 1
        self.disciplina.save()

        nova_questao = Questao(
            tipo=Questao.TIPO_VERDADEIRO_FALSO,
            enunciado='Marque se as próximas questões são verdadeiras ou falsas.'
        )
        nova_questao.save()
        nova_questao_lista = QuestaoAvaliacao(avaliacao=nova_lista, questao=nova_questao)
        nova_questao_lista.save()

        return redirect('educacao:editar_questao_avaliacao', nova_questao_lista.id, nova_lista.id)


class EditarConfiguracoesAvaliacao(EducacaoViewBasica):
    def comum(self, id_avaliacao):
        self.avaliacao = Avaliacao.objects.get(id=id_avaliacao)
        self.contexto['avaliacao'] = self.avaliacao
        self.contexto['disciplina'] = self.avaliacao.disciplina

        if self.avaliacao.tipo == self.avaliacao.AVALIACAO_LISTA_EXERCICIOS:
            self.contexto['lista'] = self.avaliacao
        elif self.avaliacao.tipo == self.avaliacao.AVALIACAO_SEMINARIO:
            self.contexto['seminario'] = self.avaliacao
        elif self.avaliacao.tipo == self.avaliacao.AVALIACAO_PROVA:
            self.contexto['prova'] = self.avaliacao
        elif self.avaliacao.tipo == self.avaliacao.AVALIACAO_TRABALHO:
            self.contexto['trabalho'] = self.avaliacao
        elif self.avaliacao.tipo == self.avaliacao.AVALIACAO_RELATORIO:
            self.contexto['relatorio'] = self.avaliacao

    def get(self, request, id_avaliacao, *args, **kwargs):
        self.comum(id_avaliacao)

        # Cria o formulário correto para lidar com o tipo de avaliação
        if self.avaliacao.tipo == self.avaliacao.AVALIACAO_LISTA_EXERCICIOS:
            self.contexto['form'] = FormEditarListaExercicios(instance=self.avaliacao)
        elif self.avaliacao.tipo == self.avaliacao.AVALIACAO_SEMINARIO:
            self.contexto['form'] = self.avaliacao
        elif self.avaliacao.tipo == self.avaliacao.AVALIACAO_PROVA:
            self.contexto['form'] = self.avaliacao
        elif self.avaliacao.tipo == self.avaliacao.AVALIACAO_TRABALHO:
            self.contexto['form'] = self.avaliacao
        elif self.avaliacao.tipo == self.avaliacao.AVALIACAO_RELATORIO:
            self.contexto['form'] = self.avaliacao

        return render(request, 'educacao/editar_configuracoes_lista_exercicios.html', self.contexto)

    def post(self, request, id_avaliacao, *args, **kwargs):
        self.comum(id_avaliacao)

        # Cria o formulário correto para lidar com o tipo de avaliação
        form = None
        if self.avaliacao.tipo == self.avaliacao.AVALIACAO_LISTA_EXERCICIOS:
            form = FormEditarListaExercicios(request.POST, instance=self.avaliacao)
        elif self.avaliacao.tipo == self.avaliacao.AVALIACAO_SEMINARIO:
            pass
        elif self.avaliacao.tipo == self.avaliacao.AVALIACAO_PROVA:
            pass
        elif self.avaliacao.tipo == self.avaliacao.AVALIACAO_TRABALHO:
            pass
        elif self.avaliacao.tipo == self.avaliacao.AVALIACAO_RELATORIO:
            pass

        if (form is not None) and form.is_valid():
            form.save()

        self.contexto['form'] = form
        return render(request, 'educacao/editar_configuracoes_lista_exercicios.html', self.contexto)


class EditarQuestaoAvaliacao(EducacaoViewBasica):
    def comum(self, id_avaliacao, id_questao_avaliacao):
        self.avaliacao = Avaliacao.objects.get(id=id_avaliacao)

        questoes_avaliacao = QuestaoAvaliacao.objects.filter(avaliacao=self.avaliacao).order_by('numero')
        if id_questao_avaliacao is None:
            self.questao_avaliacao = questoes_avaliacao.first()
        else:
            self.questao_avaliacao = QuestaoAvaliacao.objects.get(id=id_questao_avaliacao)

        self.questao = self.questao_avaliacao.questao
        self.alternativas = Alternativa.objects.filter(questao=self.questao).order_by('codigo')

        self.contexto['avaliacao'] = self.avaliacao
        self.contexto['alternativas'] = self.alternativas
        self.contexto['questao'] = self.questao
        self.contexto['questao_avaliacao'] = self.questao_avaliacao

        if self.avaliacao.tipo == self.avaliacao.AVALIACAO_LISTA_EXERCICIOS:
            self.contexto['lista'] = self.avaliacao
        elif self.avaliacao.tipo == self.avaliacao.AVALIACAO_SEMINARIO:
            self.contexto['seminario'] = self.avaliacao
        elif self.avaliacao.tipo == self.avaliacao.AVALIACAO_PROVA:
            self.contexto['prova'] = self.avaliacao
        elif self.avaliacao.tipo == self.avaliacao.AVALIACAO_TRABALHO:
            self.contexto['trabalho'] = self.avaliacao
        elif self.avaliacao.tipo == self.avaliacao.AVALIACAO_RELATORIO:
            self.contexto['relatorio'] = self.avaliacao

        # Pega a próxima questão e a questão anterior
        proxima = get_next_or_prev(questoes_avaliacao, self.questao_avaliacao, 'next')
        anterior = get_next_or_prev(questoes_avaliacao, self.questao_avaliacao, 'prev')
        self.contexto['proxima_questao'] = proxima
        self.contexto['questao_anterior'] = anterior

    def get(self, request, id_avaliacao, id_questao_avaliacao=None, *args, **kwargs):
        self.comum(id_avaliacao, id_questao_avaliacao)

        self.contexto['form_questao'] = FormEditarQuestao(instance=self.questao)

        if self.questao.tipo == self.questao.TIPO_DISSERATIVA:
            formset = FormsetEditarAlternativasDissertativa(queryset=self.alternativas)
        else:
            formset = FormsetEditarAlternativasMultiplaEscolha(queryset=self.alternativas)
        self.contexto['formset_alternativas'] = formset

        return render(request, 'educacao/editar_questao_lista_exercicios.html', self.contexto)

    def post(self, request, id_avaliacao, id_questao_avaliacao=None, *args, **kwargs):
        self.comum(id_avaliacao, id_questao_avaliacao)

        form_questao = FormEditarQuestao(request.POST, instance=self.questao)
        if self.questao.tipo == self.questao.TIPO_DISSERATIVA:
            formset = FormsetEditarAlternativasDissertativa(request.POST)
        else:
            formset = FormsetEditarAlternativasMultiplaEscolha(request.POST)

        if form_questao.is_valid():
            form_questao.save()

        for form in formset:
            if form.is_valid():
                form.save()

        if self.questao.numero_alternativas > 0:
            nota_maxima_questao = 0
            for alternativa in self.alternativas:
                nota_maxima_questao = nota_maxima_questao + alternativa.nota_maxima
            self.questao.nota_maxima = nota_maxima_questao
            self.questao.save()

        self.contexto['form_questao'] = FormEditarQuestao(instance=self.questao)
        self.contexto['formset_alternativas'] = formset

        return render(request, 'educacao/editar_questao_lista_exercicios.html', self.contexto)


class AdicionarQuestaoNaAvaliacao(EducacaoViewBasica):
    def get(self, request, id_avaliacao, *args, **kwargs):
        self.avaliacao = Avaliacao.objects.get(id=id_avaliacao)

        nova_questao = Questao(
            tipo=Questao.TIPO_VERDADEIRO_FALSO,
            enunciado='Marque se as próximas questões são verdadeiras ou falsas.'
        )
        nova_questao.save()
        nova_questao_avaliacao = QuestaoAvaliacao(
            avaliacao=self.avaliacao,
            questao=nova_questao,
            numero=self.avaliacao.numero_questoes+1
        )
        nova_questao_avaliacao.save()

        return redirect('educacao:editar_questao_avaliacao', nova_questao_avaliacao.id, self.avaliacao.id)


class RemoverQuestaoAvaliacao(EducacaoViewBasica):
    def get(self, request, id_questao_avaliacao, *args, **kwargs):
        questao_avaliacao = QuestaoAvaliacao.objects.get(id=id_questao_avaliacao)
        avaliacao = questao_avaliacao.avaliacao
        questoes_avaliacao = QuestaoAvaliacao.objects.filter(avaliacao=avaliacao)
        alternativas = Alternativa.objects.filter(questao=questao_avaliacao.questao)
        respostas = Resposta.objects.filter(
            questao__avaliacao__questaoavaliacao=questao_avaliacao
        )

        proxima = get_next_or_prev(questoes_avaliacao, questao_avaliacao, 'next')
        anterior = get_next_or_prev(questoes_avaliacao, questao_avaliacao, 'prev')

        respostas.delete()
        questao_avaliacao.delete()
        alternativas.delete()
        questao_avaliacao.questao.delete()

        if anterior is not None:
            return redirect('educacao:editar_questao_avaliacao', anterior.id, avaliacao.id)
        elif proxima is not None:
            return redirect('educacao:editar_questao_avaliacao', proxima.id, avaliacao.id)
        else:
            return redirect('educacao:ver_disciplina', avaliacao.disciplina.id)


class AdicionarAlternativaNaQuestaoDaLista(EducacaoViewBasica):
    def get(self, request, id_questao_avaliacao, *args, **kwargs):
        self.questao_avaliacao = QuestaoAvaliacao.objects.get(id=id_questao_avaliacao)
        questao = self.questao_avaliacao.questao

        alternativas = Alternativa.objects.filter(questao=self.questao_avaliacao.questao)
        n_alternativas = alternativas.count()

        nova_alternativa = Alternativa(
            questao=self.questao_avaliacao.questao,
            codigo=chr(ord('a') + n_alternativas),
            enunciado='Por favor preencha o enunciado.'
        )
        nova_alternativa.save()

        # Atualiza a nota máxima da questão e a quantidade de alternativas
        alternativas = Alternativa.objects.filter(questao=self.questao_avaliacao.questao)
        soma_notas_alternativas = alternativas.aggregate(nota=Sum('nota_maxima'))['nota']
        questao.nota_maxima = soma_notas_alternativas
        questao.save()

        id_avaliacao = self.questao_avaliacao.avaliacao.id
        return redirect('educacao:editar_questao_avaliacao', id_questao_avaliacao, id_avaliacao)


class ResponderQuestaoListaExercicios(EducacaoViewBasica):
    def comum(self, id_avaliacao, id_questao_avaliacao):
        self.avaliacao = Avaliacao.objects.get(id=id_avaliacao)
        self.contexto['avaliacao'] = self.avaliacao

        questoes_avaliacao = QuestaoAvaliacao.objects.filter(avaliacao=self.avaliacao).order_by('numero')
        if id_questao_avaliacao is None:
            self.questao_avaliacao = questoes_avaliacao.first()
        else:
            self.questao_avaliacao = QuestaoAvaliacao.objects.get(id=id_questao_avaliacao)

        self.contexto['questao_avaliacao'] = self.questao_avaliacao
        self.questao = self.questao_avaliacao.questao
        self.contexto['questao'] = self.questao
        self.alternativas = Alternativa.objects.filter(questao=self.questao_avaliacao.questao).order_by('codigo')
        self.contexto['alternativas'] = self.alternativas

        # Pega a próxima questão e a questão anterior
        questoes_avaliacao = QuestaoAvaliacao.objects.filter(avaliacao=self.avaliacao).order_by('numero')
        self.contexto['questoes_avaliacao'] = questoes_avaliacao
        proxima = get_next_or_prev(questoes_avaliacao, self.questao_avaliacao, 'next')
        anterior = get_next_or_prev(questoes_avaliacao, self.questao_avaliacao, 'prev')
        self.contexto['proxima_questao'] = proxima
        self.contexto['questao_anterior'] = anterior

    def carregar_respostas(self):
        # Garante a existência de respostas para as alternativas da questão
        if self.alternativas.count() > 0:
            for alternativa in self.alternativas:
                resposta = Resposta.objects.filter(
                    questao=self.questao_avaliacao.questao,
                    questao_avaliacao=self.questao_avaliacao,
                    alternativa=alternativa,
                    disciplina=self.avaliacao.disciplina,
                    avaliacao=self.avaliacao,
                    aluno=self.usuario,
                )

                if resposta.count() == 0:
                    nova_resposta = Resposta(
                        questao=self.questao_avaliacao.questao,
                        questao_avaliacao=self.questao_avaliacao,
                        alternativa=alternativa,
                        disciplina=self.avaliacao.disciplina,
                        avaliacao=self.avaliacao,
                        aluno=self.usuario
                    )
                    nova_resposta.save()

            self.respostas = Resposta.objects.filter(
                questao_avaliacao=self.questao_avaliacao,
                questao=self.questao_avaliacao.questao,
                alternativa__in=self.alternativas,
                aluno=self.usuario
            )

        # caso em que a pergunta não tem alternativas
        # (geralmente questões dissertativas)
        else:
            resposta = Resposta.objects.filter(
                questao=self.questao_avaliacao.questao,
                questao_avaliacao=self.questao_avaliacao,
                disciplina=self.avaliacao.disciplina,
                avaliacao=self.avaliacao,
                aluno=self.usuario,
            )

            if resposta.count() == 0:
                nova_resposta = Resposta(
                    questao=self.questao_avaliacao.questao,
                    questao_avaliacao=self.questao_avaliacao,
                    disciplina=self.avaliacao.disciplina,
                    avaliacao=self.avaliacao,
                    aluno=self.usuario,
                )
                nova_resposta.save()

            self.respostas = Resposta.objects.filter(
                questao_avaliacao=self.questao_avaliacao,
                questao=self.questao_avaliacao.questao,
                aluno=self.usuario
            )

        # Garante a existência da nota do aluno para a avaliação
        nota_aluno = NotaAlunoAvaliacao.objects.filter(
            aluno=self.usuario,
            avaliacao=self.avaliacao
        )
        if nota_aluno.count() == 0:
            nota_aluno = NotaAlunoAvaliacao(
                aluno=self.usuario,
                avaliacao=self.avaliacao
            )
            nota_aluno.save()

    def get(self, request, id_avaliacao, id_questao_avaliacao=None, *args, **kwargs):
        self.comum(id_avaliacao, id_questao_avaliacao)
        self.carregar_respostas()

        formset = None
        if self.questao.tipo == self.questao.TIPO_DISSERATIVA:
            formset = FormEditarRespostaDissertativa(
                instance=self.respostas.first()
            )

        elif self.questao.tipo == self.questao.TIPO_VERDADEIRO_FALSO:
            formset = FormsetEditarRespostaMultiplaEscolha(
                opcoes=self.alternativas[0].OPCOES,
                queryset=self.respostas
            )

        elif self.questao.tipo == self.questao.TIPO_MULTIPLA_ESCOLHA:
            # TODO adicionar suporte para questões multipla escolha
            pass

        anexos = AnexoResposta.objects.filter(resposta__in=self.respostas)
        self.contexto['anexos'] = anexos
        ids_respostas_com_anexo = anexos.values_list('resposta__id', flat=True)
        self.contexto['ids_respostas_com_anexo'] = ids_respostas_com_anexo

        self.contexto['formset_respostas'] = formset
        return render(request, 'educacao/resolver_lista_exercicios.html', self.contexto)

    def post(self, request, id_avaliacao, id_questao_avaliacao=None, *args, **kwargs):
        self.comum(id_avaliacao, id_questao_avaliacao)
        self.carregar_respostas()

        if self.questao.tipo == self.questao.TIPO_DISSERATIVA:
            form = FormEditarRespostaDissertativa(
                request.POST,
                instance=self.respostas.first()
            )

            if form.is_valid():
                resposta = form.save(commit=False)

                # o aluno respondeu a questão?
                if resposta.conteudo != '' or resposta.numero_anexos > 0:
                    resposta.respondida = True
                else:
                    resposta.respondida = False

                if form.has_changed():
                    resposta.data_submissao = datetime.datetime.now()
                    resposta.numero_tentativas = resposta.numero_tentativas + 1
                resposta.save()

            self.contexto['formset_respostas'] = form

        elif self.questao.tipo == self.questao.TIPO_VERDADEIRO_FALSO:
            formset = FormsetEditarRespostaMultiplaEscolha(
                request.POST,
                opcoes=self.alternativas[0].OPCOES,
                queryset=self.respostas
            )

            if formset.is_valid():
                for form in formset:
                    resposta = form.save(commit=False)

                    # O aluno respondeu a questão?
                    if resposta.resposta != '' or resposta.numero_anexos > 0:
                        resposta.respondida = True
                    else:
                        resposta.respondida = False

                    if form.has_changed():
                        resposta.data_submissao = datetime.datetime.now()
                        resposta.numero_tentativas = resposta.numero_tentativas + 1
                        resposta.save()

                        # Corrige automaticamente a questão
                        if self.avaliacao.correcao_automatica:
                            resp_correta = resposta.alternativa.resposta_correta
                            if resposta.resposta == resp_correta:
                                resposta.nota = resposta.alternativa.nota_maxima
                            else:
                                resposta.nota = 0
                            resposta.corrigido = True
                            resposta.data_correcao = datetime.datetime.now()

                        self.questao.numero_respostas = self.questao.numero_respostas + 1
                        resposta.save()

                        # Atualiza as estatísticas
                        respostas = Resposta.objects.filter(questao=self.questao)
                        analise = respostas.aggregate(media=Avg('nota'), desvio_padrao=StdDev('nota'))
                        self.questao.nota_media = analise['media']
                        self.questao.desvio_padrao_nota = analise['desvio_padrao']
                        self.questao.save()

            self.contexto['formset_respostas'] = formset

        elif self.questao.tipo == self.questao.TIPO_MULTIPLA_ESCOLHA:
            pass

        return render(request, 'educacao/resolver_lista_exercicios.html', self.contexto)


class AdicionarAnexo(EducacaoViewBasica):
    def get(self, request, onde, id, *args, **kwargs):
        form = None
        if onde == 'resposta':  # veio o id de uma questão de avaliação
            questao_avaliacao = QuestaoAvaliacao.objects.get(id=id)
            respostas = Resposta.objects.filter(
                questao_avaliacao=questao_avaliacao,
                aluno=self.usuario
            )
            ids_respostas = list(respostas.values_list('id', flat=True))
            form = FormAdicionarAnexoResposta(ids_respostas=ids_respostas)
            self.contexto['onde'] = 'resposta'
            self.contexto['questao_avaliacao'] = questao_avaliacao

        elif onde == 'relatorio':
            relatorio = Avaliacao.objects.get(id=id)
            form = FormAdicionarAnexoRelatorio()
            self.contexto['onde'] = 'relatorio'
            self.contexto['relatorio'] = relatorio

        self.contexto['form'] = form
        return render(request, 'educacao/adicionar_anexo.html', self.contexto)

    def post(self, request, onde, id, *args, **kwargs):
        if onde == 'resposta':
            questao_avaliacao = QuestaoAvaliacao.objects.get(id=id)
            id_questao = questao_avaliacao.id
            id_avaliacao = questao_avaliacao.avaliacao.id
            respostas = Resposta.objects.filter(
                questao_avaliacao=questao_avaliacao,
                aluno=self.usuario
            )
            ids_respostas = list(respostas.values_list('id', flat=True))

            form = FormAdicionarAnexoResposta(
                request.POST,
                request.FILES,
                ids_respostas=ids_respostas
            )

            if form.is_valid():
                form.save()

            return redirect('educacao:responder_questao_da_avaliacao', id_questao, id_avaliacao)

        elif onde == 'relatorio':
            relatorio = Avaliacao.objects.get(id=id)
            self.contexto['onde'] = 'relatorio'
            self.contexto['relatorio'] = relatorio

            form = FormAdicionarAnexoRelatorio(
                request.POST,
                request.FILES,
            )

            if form.is_valid():
                novo_anexo = form.save(commit=False)
                novo_anexo.relatorio = relatorio
                novo_anexo.save()

            return redirect('educacao:responder_relatorio', relatorio.id)


class RemoverAnexoOnde(EducacaoViewBasica):
    def get(self, request, onde, id_anexo, *args, **kwargs):
        if onde == 'resposta':
            anexo = AnexoResposta.objects.get(id=id_anexo)
            resposta = anexo.resposta
            id_questao = resposta.questao_avaliacao.id
            id_avaliacao = resposta.avaliacao.id
            anexo.delete()
            return redirect('educacao:responder_questao_da_avaliacao', id_questao, id_avaliacao)

        elif onde == 'relatorio':
            anexo = AnexoRelatorio.objects.get(id=id_anexo)
            id_relatorio = anexo.relatorio.id
            anexo.delete()
            return redirect('educacao:responder_relatorio', id_relatorio)


class VerCorrecaoAvaliacao(EducacaoViewBasica):
    def get(self, request, id_avaliacao, *args, **kwargs):
        avaliacao = Avaliacao.objects.get(id=id_avaliacao)
        qs = QuestaoAvaliacao.objects.filter(avaliacao=avaliacao).order_by('numero')

        questoes = list()
        for questao in qs:
            aux = dict()
            resps = RespostaAlternativa.objects.filter(
                aluno=self.usuario,
                questao__questaolistaexercicios=questao
            )
            aux['respostas'] = resps
            aux['questao'] = questao
            questoes.append(aux)

        self.contexto['questoes'] = questoes
        self.contexto['lista'] = lista
        self.contexto['aula'] = lista.aula
        return render(request, 'educacao/ver_correcao.html', self.contexto)


class VerEntregasAvaliacao(EducacaoViewBasica):
    def get(self, request, id_avaliacao, *args, **kwargs):
        avaliacao = Avaliacao.objects.get(id=id_avaliacao)
        self.contexto['avaliacao'] = avaliacao

        alunos_que_entregaram = Usuario.objects.filter(
            resposta__avaliacao=avaliacao,
            resposta__data_submissao__isnull=False
        ).distinct()
        self.contexto['alunos_que_entregaram'] = alunos_que_entregaram

        entregas = list()
        for aluno in alunos_que_entregaram:
            aux = dict()

            aux['aluno'] = aluno
            aux['nota'] = NotaAlunoAvaliacao.objects.get(
                aluno=aluno,
                avaliacao=avaliacao
            )

            entregas.append(aux)
        self.contexto['entregas'] = entregas

        return render(request, 'educacao/ver_entregas_avaliacao.html', self.contexto)


class CorrigirQuestaoAvaliacao(EducacaoViewBasica):
    def comum(self, id_avaliacao, id_aluno):
        self.avaliacao = Avaliacao.objects.get(id=id_avaliacao)
        self.aluno = Usuario.objects.get(id=id_aluno)
        self.respostas = Resposta.objects.filter(
            avaliacao=self.avaliacao,
            aluno=self.aluno
        ).order_by('questao_avaliacao__numero', 'alternativa__codigo')

        questoes = Questao.objects.filter(
            questaoavaliacao__avaliacao=self.avaliacao
        ).distinct().order_by('questaoavaliacao__numero')

        objetos = list()
        for questao in questoes:
            aux = dict()
            aux['questao'] = questao
            aux['questao_avaliacao'] = QuestaoAvaliacao.objects.get(
                questao=questao,
                avaliacao=self.avaliacao
            )
            aux['alternativas'] = Alternativa.objects.filter(
                questao=questao
            ).order_by('codigo')
            objetos.append(aux)

        self.contexto['questoes'] = objetos

        self.contexto['anexos'] = AnexoResposta.objects.filter(
            resposta__aluno=self.aluno,
            resposta__avaliacao=self.avaliacao
        )

    def get(self, request, id_avaliacao, id_aluno, *args, **kwargs):
        self.comum(id_avaliacao, id_aluno)
        correcoes = FabricaCorrigirResposta(queryset=self.respostas)
        self.contexto['correcoes'] = correcoes

        return render(request, 'educacao/corrigir_avaliacao.html', self.contexto)

    def post(self, request, id_avaliacao, id_aluno, *args, **kwargs):
        self.comum(id_avaliacao, id_aluno)

        forms_correcoes = FabricaCorrigirResposta(
            request.POST,
            queryset=self.respostas
        )

        # Calcula a nota do estudante
        nota = 0
        for form in forms_correcoes:
            if form.is_valid():
                form.save()

                nota_atribuida = form.instance.nota
                if form.instance.questao.numero_alternativas > 0:
                    if nota_atribuida > form.instance.alternativa.nota_maxima:
                        nota_atribuida = form.instance.alternativa.nota_maxima
                        form.instance.nota = nota_atribuida
                        form.instance.save()
                else:
                    if nota_atribuida > form.instance.questao.nota_maxima:
                        nota_atribuida = form.instance.questao.nota_maxima
                        form.instance.nota = nota_atribuida
                        form.instance.save()

                nota = nota + nota_atribuida

        # Lida com os dias de atraso
        if self.avaliacao.penalidade_por_dia_atraso > 0:
            datas_entrega = Resposta.objects.filter(
                avaliacao=self.avaliacao,
                aluno=self.aluno
            ).order_by('data_submissao').values_list('data_submissao', flat=True)
            data_entrega = datas_entrega.last()
            data_limite = self.avaliacao.data_limite_entrega
            if datas_entrega is not None and data_limite is not None:
                dias_de_atraso = data_entrega - data_limite
                dias_de_atraso = dias_de_atraso.days if dias_de_atraso.days >= 0 else 0
                nota_final = nota - dias_de_atraso
                nota = nota_final if nota_final > 0 else 0

        # TODO adicionar suporte para descontar pontos por questão errada
        if self.avaliacao.descontar_um_ponto_a_cada_n_erradas > 0:
            pass

        registro_nota = NotaAlunoAvaliacao.objects.filter(
            aluno=self.aluno,
            avaliacao=self.avaliacao
        )

        if registro_nota.count() == 1:
            registro_nota = registro_nota.first()
            registro_nota.nota = nota
        else:
            registro_nota = NotaAlunoAvaliacao(
                aluno=self.aluno,
                avaliacao=self.avaliacao,
                nota=nota
            )
        registro_nota.save()

        forms_correcoes = FabricaCorrigirResposta(queryset=self.respostas)
        self.contexto['correcoes'] = forms_correcoes

        return render(request, 'educacao/corrigir_avaliacao.html', self.contexto)


class CriarRelatorio(EducacaoViewBasica):
    def get(self, request, id_disciplina, *args, **kwargs):
        self.disciplina = Disciplina.objects.get(id=id_disciplina)
        self.relatorios = Avaliacao.objects.filter(
            disciplina=self.disciplina,
            tipo=Avaliacao.AVALIACAO_RELATORIO,
        )

        numero_relatorio = self.relatorios.count() + 1
        novo_relatorio = Avaliacao(
            tipo=Avaliacao.AVALIACAO_RELATORIO,
            individual=False,
            numero_maximo_integrantes_grupo=3,
            titulo='Relatório %d' % numero_relatorio,
            numero=numero_relatorio,
            numero_questoes=0,
            disciplina=self.disciplina,
        )
        novo_relatorio.save()
        self.contexto['relatorio'] = novo_relatorio

        # Aumenta o número de listas de exercícios da disciplina
        self.disciplina.numero_relatorios = self.disciplina.numero_relatorios + 1
        self.disciplina.save()

        return redirect('educacao:editar_relatorio', novo_relatorio.id)


class AdicionarExperimentoRemotoNoRelatorio(EducacaoViewBasica):
    def get(self, request, id_relatorio, *args, **kwargs):
        relatorio = Avaliacao.objects.get(id=id_relatorio)

        # Cria o novo ensiao
        novo_ensaio = Ensaio(
            professor=self.usuario,
            nome='Meu novo ensaio experimental',
        )
        novo_ensaio.save()

        # Cria os parâmetros:
        # - período de amostragem (Ts) e
        # - duração do experimento (tend)
        parametro_Ts = ParametroParaSerInformado(
            nome='Período de amostragem (segundos)',
            ensaio=novo_ensaio,
            sigla='Ts',
            valor_minimo=0.01,
            valor_maximo=10.0,
            pode_ser_removido=False,
        )
        parametro_Ts.save()

        parametro_tend = ParametroParaSerInformado(
            nome='Duração do experimento (segundos)',
            ensaio=novo_ensaio,
            sigla='tend',
            valor_minimo=0,
            valor_maximo=30,
            pode_ser_removido=False,
        )
        parametro_tend.save()

        # Associa ele com o relatório
        experimento = ExperimentoLabRemoto(
            avaliacao=relatorio,
            ensaio=novo_ensaio,
        )
        experimento.save()

        return redirect('lab_remoto:editar_ensaio', novo_ensaio.id)


class RemoverExperimentoNoLabRemoto(EducacaoViewBasica):
    def get(self, request, id_experimento, *args, **kwargs):
        experimento = ExperimentoLabRemoto.objects.get(id=id_experimento)
        ensaio = experimento.ensaio
        parametros = ParametroParaSerInformado.objects.filter(ensaio=ensaio)
        id_relatorio = experimento.avaliacao.id

        experimento.delete()
        parametros.delete()
        ensaio.delete()

        return redirect('educacao:editar_relatorio', id_relatorio)


class EditarRelatorio(EducacaoViewBasica):
    def comum(self, id_relatorio):
        self.relatorio = Avaliacao.objects.get(id=id_relatorio)
        self.contexto['relatorio'] = self.relatorio

        experimentos = ExperimentoLabRemoto.objects.filter(
            avaliacao=self.relatorio,
        )
        self.contexto['experimentos'] = experimentos

    def get(self, request, id_relatorio, *args, **kwargs):
        self.comum(id_relatorio)

        formulario = FormEditarRelatorio(instance=self.relatorio)
        self.contexto['formulario'] = formulario

        return render(request, 'educacao/editar_relatorio.html', self.contexto)

    def post(self, request, id_relatorio, *args, **kwargs):
        self.comum(id_relatorio)

        formulario = FormEditarRelatorio(
            request.POST,
            request.FILES,
            instance=self.relatorio,
        )

        if formulario.is_valid():
            formulario.save()
            if formulario.cleaned_data['numero_maximo_integrantes_grupo'] > 1:
                self.relatorio.individual = False
            else:
                self.relatorio.individual = True
            self.relatorio.save()
            formulario = FormEditarRelatorio(instance=self.relatorio)

        self.contexto['formulario'] = formulario
        return render(request, 'educacao/editar_relatorio.html', self.contexto)


class ResponderRelatorio(EducacaoViewBasica):
    def get(self, request, id_relatorio, *args, **kwargs):
        relatorio = Avaliacao.objects.get(id=id_relatorio)
        experimentos = ExperimentoLabRemoto.objects.filter(avaliacao=relatorio)
        anexos = AnexoRelatorio.objects.filter(relatorio=relatorio)

        self.contexto['relatorio'] = relatorio
        self.contexto['experimentos'] = experimentos
        self.contexto['anexos'] = anexos

        if self.usuario.tipo == self.usuario.TIPO_DOCENTE:
            # Tabela com o número de execuções dos alunos
            tabela_acompanhamento = list()
            alunos = relatorio.disciplina.inscritos.all().order_by('usuario_django__first_name')
            for aluno in alunos:
                dados = dict()
                dados['aluno'] = aluno
                dados['contagem'] = ExecucaoEnsaio.objects.filter(
                    usuario=aluno,
                    ensaio__experimentolabremoto__in=experimentos,
                    estado=ExecucaoEnsaio.ESTADO_FINALIZADO,
                    # data_inicio__lt=datetime.datetime(2020, 11, 27),
                ).distinct().count()

                tabela_acompanhamento.append(dados)

            self.contexto['tabela_execucoes'] = tabela_acompanhamento

            # Tabela com o número de execuções por dia
            data_inicio = relatorio.data_para_ficar_visivel
            data_fim = relatorio.data_limite_entrega
            n_dias = (data_fim - data_inicio).days
            datas = [data_inicio + datetime.timedelta(days=x) for x in range(n_dias + 1)]
            tabela_execucoes_por_data = list()
            for data in datas:
                qtd = ExecucaoEnsaio.objects.filter(
                    ensaio__experimentolabremoto__in=experimentos,
                    estado=ExecucaoEnsaio.ESTADO_FINALIZADO,
                    data_inicio__year=data.year,
                    data_inicio__month=data.month,
                    data_inicio__day=data.day,
                ).distinct().count()

                execucoes_por_data = {
                    'data': data,
                    'qtd': qtd,
                }
                tabela_execucoes_por_data.append(execucoes_por_data)

            self.contexto['tabela_execucoes_por_data'] = tabela_execucoes_por_data

        return render(request, 'educacao/responder_relatorio.html', self.contexto)


class VerExperimento(EducacaoViewBasica):
    def get(self, request, id_experimento, *args, **kwargs):
        experimento = ExperimentoLabRemoto.objects.get(id=id_experimento)
        self.contexto['experimento'] = experimento

        execucoes = ExecucaoEnsaio.objects.filter(
            usuario=self.usuario,
            ensaio=experimento.ensaio,
        ).order_by('data_inicio')
        self.contexto['execucoes'] = execucoes

        return render(request, 'educacao/ver_experimento.html', self.contexto)


class CriarExecucao(EducacaoViewBasica):
    def get(self, request, id_experimento, *args, **kwargs):
        experimento = ExperimentoLabRemoto.objects.get(id=id_experimento)
        parametros_a_serem_informados = ParametroParaSerInformado.objects.filter(ensaio=experimento.ensaio)

        execucoes = ExecucaoEnsaio.objects.filter(
            usuario=self.usuario,
            ensaio=experimento.ensaio,
        )

        # Estados em que o sistema não permite que a pessoa crie novas execuções
        lista_nao_pode = (
            ExecucaoEnsaio.ESTADO_EXECUTANDO,
            ExecucaoEnsaio.ESTADO_NA_FILA,
            ExecucaoEnsaio.ESTADO_PARAMETROS_PREENCHIDOS,
            ExecucaoEnsaio.ESTADO_AGUARDANDO_PARAMETROS,
        )

        # Pode criar uma nova execucação?
        # - Caso exista alguma execucação em algum dos estados acima, então o usuário não pode
        #   criar novas execuções, ele deve esperar o ensaio terminar
        ip = get_client_ip(request)
        if execucoes.filter(estado__in=lista_nao_pode).count() == 0:
            execucao = ExecucaoEnsaio(
                usuario=self.usuario,
                kit=experimento.ensaio.kit,
                ensaio=experimento.ensaio,
                ip_quem_criou=ip,
            )
            execucao.save()

            # Agora vou criar os parâmetros que precisam ser informados
            for parametro in parametros_a_serem_informados:
                novo_parametro_a_ser_informado = ParametroInformado(
                    usuario=self.usuario,
                    ensaio=experimento.ensaio,
                    execucao=execucao,
                    parametro=parametro,
                    valor_informado=parametro.valor_minimo,
                )
                novo_parametro_a_ser_informado.save()

            return redirect('educacao:editar_execucao', id_experimento, execucao.id)

        else:
            return redirect('educacao:ver_experimento', id_experimento)


class EditarExecucao(EducacaoViewBasica):
    def comum(self, id_experimento, id_execucao):
        self.experimento = ExperimentoLabRemoto.objects.get(id=id_experimento)
        self.execucao = ExecucaoEnsaio.objects.get(id=id_execucao)
        self.parametros_informados = ParametroInformado.objects.filter(
            usuario=self.usuario,
            execucao=self.execucao,
        )
        self.contexto['experimento'] = self.experimento
        self.contexto['execucao'] = self.execucao
        self.contexto['parametros_informados'] = self.parametros_informados

    def get(self, request, id_experimento, id_execucao, *args, **kwargs):
        self.comum(id_experimento, id_execucao)
        formularios_parametros = FormsetEditarParametrosInformados(
            queryset=self.parametros_informados
        )
        self.contexto['formularios_parametros'] = formularios_parametros

        formulario_filmar = FormFilmarExperimento(instance=self.execucao)
        self.contexto['formulario_filmar'] = formulario_filmar

        return render(request, 'educacao/editar_execucao.html', self.contexto)

    def post(self, request, id_experimento, id_execucao, *args, **kwargs):
        self.comum(id_experimento, id_execucao)

        formulario_filmar = FormFilmarExperimento(
            request.POST,
            request.FILES,
            instance=self.execucao,
        )
        if formulario_filmar.is_valid():
            formulario_filmar.save()
        formulario_filmar = FormFilmarExperimento(instance=self.execucao)
        self.contexto['formulario_filmar'] = formulario_filmar

        formset = FormsetEditarParametrosInformados(
            request.POST,
            request.FILES,
            queryset=self.parametros_informados,
        )

        todos_parametros_ok = True
        for formulario in formset:
            if formulario.is_valid():
                pi = formulario.instance  # pega o parametro informado
                valor_min = pi.parametro.valor_minimo
                valor_max = pi.parametro.valor_maximo
                valor_informado = formulario.cleaned_data['valor_informado']
                if valor_min <= valor_informado <= valor_max:
                    formulario.save()
                else:
                    todos_parametros_ok = False
            else:
                todos_parametros_ok = False

        # Se necessário atualiza o estado da execução
        if todos_parametros_ok is True:
            self.execucao.estado = self.execucao.ESTADO_PARAMETROS_PREENCHIDOS
            self.execucao.save()

        # Atualiza os campos dos formulários
        parametros_informados = ParametroInformado.objects.filter(
            usuario=self.usuario,
            execucao=self.execucao,
        )
        formularios_parametros = FormsetEditarParametrosInformados(queryset=parametros_informados)
        self.contexto['formularios_parametros'] = formularios_parametros

        return render(request, 'educacao/editar_execucao.html', self.contexto)


class DuplicarExecucao(EducacaoViewBasica):
    def get(self, request, id_experimento, id_execucao, *args, **kwargs):
        execucao = ExecucaoEnsaio.objects.get(id=id_execucao)
        execucoes = ExecucaoEnsaio.objects.filter(
            usuario=self.usuario,
            ensaio=execucao.ensaio,
        )

        # Estados em que o sistema não permite que a pessoa crie novas execuções
        lista_nao_pode = (
            ExecucaoEnsaio.ESTADO_EXECUTANDO,
            ExecucaoEnsaio.ESTADO_NA_FILA,
            ExecucaoEnsaio.ESTADO_PARAMETROS_PREENCHIDOS,
            ExecucaoEnsaio.ESTADO_AGUARDANDO_PARAMETROS,
        )

        # Pode criar uma nova execucação?
        # - Caso exista alguma execucação em algum dos estados acima, então o usuário não pode
        #   criar novas execuções, ele deve esperar o ensaio terminar
        ip = get_client_ip(request)
        if execucoes.filter(estado__in=lista_nao_pode).count() == 0:
            copia_execucao = ExecucaoEnsaio(
                usuario=self.usuario,
                kit=execucao.kit,
                ensaio=execucao.ensaio,
                ip_quem_criou=ip,
                filmar=execucao.filmar,
            )
            copia_execucao.save()

            # Agora vou copiar os parâmetros informados
            parametros_informados = ParametroInformado.objects.filter(
                execucao=execucao,
            )

            for parametro in parametros_informados:
                copia_parametro_informado = ParametroInformado(
                    usuario=self.usuario,
                    ensaio=parametro.ensaio,
                    execucao=copia_execucao,
                    parametro=parametro.parametro,
                    valor_informado=parametro.valor_informado,
                )
                copia_parametro_informado.save()

            return redirect('educacao:editar_execucao', id_experimento, copia_execucao.id)

        else:
            return redirect('educacao:ver_experimento', id_experimento)


class VerExecucao(EducacaoViewBasica):
    def get(self, request, id_experimento, id_execucao, *args, **kwargs):
        experimento = ExperimentoLabRemoto.objects.get(id=id_experimento)
        execucao = ExecucaoEnsaio.objects.get(id=id_execucao)
        parametros_informados = ParametroInformado.objects.filter(
            usuario=self.usuario,
            execucao=execucao,
        )
        self.contexto['experimento'] = experimento
        self.contexto['execucao'] = execucao
        self.contexto['parametros_informados'] = parametros_informados

        form_tag = FormEditarTagExecucao(instance=execucao)
        self.contexto['form_tag'] = form_tag

        return render(request, 'educacao/ver_execucao.html', self.contexto)

    def post(self, request, id_experimento, id_execucao, *args, **kwargs):
        experimento = ExperimentoLabRemoto.objects.get(id=id_experimento)
        execucao = ExecucaoEnsaio.objects.get(id=id_execucao)
        parametros_informados = ParametroInformado.objects.filter(
            usuario=self.usuario,
            execucao=execucao,
        )
        self.contexto['experimento'] = experimento
        self.contexto['execucao'] = execucao
        self.contexto['parametros_informados'] = parametros_informados

        form_tag = FormEditarTagExecucao(
            request.POST,
            request.FILES,
            instance=execucao,
        )
        if form_tag.is_valid():
            form_tag.save()
        form_tag = FormEditarTagExecucao(instance=execucao)
        self.contexto['form_tag'] = form_tag

        return render(request, 'educacao/ver_execucao.html', self.contexto)


class VerCodigoExecucao(EducacaoViewBasica):
    def get(self, request, id_experimento, id_execucao, *args, **kwargs):
        execucao = ExecucaoEnsaio.objects.get(id=id_execucao)
        experimento = ExperimentoLabRemoto.objects.get(id=id_experimento)
        self.contexto['execucao'] = execucao
        self.contexto['experimento'] = experimento

        return render(request, 'educacao/ver_codigo_execucao.html', self.contexto)


class VerGrafico(EducacaoViewBasica):
    def get(self, request, id_experimento, id_execucao, *args, **kwargs):
        experimento = ExperimentoLabRemoto.objects.get(id=id_experimento)
        execucao = ExecucaoEnsaio.objects.get(id=id_execucao)
        self.contexto['experimento'] = experimento
        self.contexto['execucao'] = execucao

        # Carrega os dados do arquivo matlab
        arquivo = os.path.join(settings.MEDIA_ROOT, execucao.arquivo.name)
        dados = loadmat(arquivo, squeeze_me=True)

        # Cria a figura
        codigo_grafico = execucao.ensaio.codigo_grafico

        try:
            plot = figure(
                x_axis_label='Tempo (s)',
                plot_width=640,
                plot_height=480
            )

            exec(codigo_grafico)

            script, div = components(plot)
            self.contexto['script'] = script
            self.contexto['div'] = div

        except:
            plot = None

        return render(request, 'educacao/ver_grafico.html', self.contexto)


class ExecutarExperimento(EducacaoViewBasica):
    def get(self, request, id_experimento, id_execucao, *args, **kwargs):
        execucao = ExecucaoEnsaio.objects.get(id=id_execucao)

        # Só pode agendar uma nova execucação se a execução estiver com os parâmetros preenchidos
        if execucao.estado == execucao.ESTADO_PARAMETROS_PREENCHIDOS:
            # Cria a task do Celery para rodar o experimento
            execucao.estado = execucao.ESTADO_NA_FILA

            # Armazena o IP de quem clicou em executar
            ip = get_client_ip(request)
            execucao.ip_quem_executou = ip

            execucao.save()

            # Agenda a execucação do experimento
            executar_experimento_assincrono.delay(id_experimento, id_execucao)
            # executar_experimento(id_experimento, id_execucao)

        return redirect('educacao:ver_experimento', id_experimento)

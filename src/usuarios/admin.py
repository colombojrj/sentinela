from django.contrib import admin
from .models import *


class AdminArea(admin.ModelAdmin):
    list_display = ('nome', )


class AdminUsuario(admin.ModelAdmin):
    list_display = ('tipo', 'prontuario', )


admin.site.register(Area, AdminArea)
admin.site.register(Usuario, AdminUsuario)


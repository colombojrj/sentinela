from django.urls import path
from . import views

urlpatterns = [
    path(
        '',
        views.pagina_entrar,
        name='pagina_entrar'
    ),
    path(
        'usuario_criado/',
        views.pagina_entrar_usuario_criado,
        name='pagina_entrar_usuario_criado'
    ),
    path(
        'criar_conta/',
        views.pagina_criar_conta,
        name='pagina_criar_conta'
    ),
    path(
        'entrar/',
        views.pagina_entrar,
        name='pagina_entrar'
    ),
    path(
        'sair/',
        views.pagina_sair,
        name='pagina_sair'
    ),
]

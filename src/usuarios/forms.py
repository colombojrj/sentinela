from django import forms
from django.contrib.auth.models import User
from .models import Usuario


class FormularioEntrar(forms.Form):
    prontuario = forms.CharField(
        label='',
        max_length=200,
        required=True,
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Prontuário',
                'class': 'form-control'
            }
        )
    )

    senha = forms.CharField(
        label='',
        widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Senha',
                'class': 'form-control'
            }
        )
    )


class FormularioCriarConta(forms.Form):
    primeiro_nome = forms.CharField(
        label='Primeiro nome',
        max_length=200,
        required=True,
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Primeiro nome',
                'class': 'form-control'
            }
        )
    )

    sobrenome = forms.CharField(
        label='Sobrenome',
        max_length=200,
        required=True,
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Sobrenome',
                'class': 'form-control'
            }
        )
    )

    prontuario = forms.CharField(
        label='Prontuário',
        max_length=20,
        required=True,
        widget=forms.TextInput(
            attrs={
                'placeholder': 'Exemplo: SJ0123456789',
                'class': 'form-control'
            }
        )
    )

    email = forms.CharField(
        label='E-mail',
        max_length=200,
        required=True,
        widget=forms.EmailInput(
            attrs={
                'placeholder': 'E-mail',
                'class': 'form-control'
            }
        )
    )

    senha = forms.CharField(
        label='Senha',
        widget=forms.PasswordInput(
            attrs={
                'placeholder': 'Senha',
                'class': 'form-control'}
        )
    )
    




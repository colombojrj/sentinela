from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.views import View

import datetime

from . import forms
from .models import Usuario


class SentinelaViewBasica(View):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.texto_aviso_verde = None
        self.texto_aviso_vermelho = None
        self.contexto = dict()

    def setup(self, request, *args, **kwargs):
        super().setup(request, *args, **kwargs)

        self.usuario = self.selecionar_usuario(request)
        # self.instituicao = None
        self.contexto['usuario'] = self.usuario

    def selecionar_usuario(self, request):
        usuario = None, None

        usuario_django = request.user
        query_usuario = Usuario.objects.filter(usuario_django=usuario_django)
        if query_usuario.count() == 1:
            usuario = query_usuario[0]

        return usuario


def pagina_entrar_usuario_criado(request):
    return pagina_entrar(request, usuario_criado=True)


def pagina_entrar(request, usuario_criado=None):
    form = forms.FormularioEntrar()
    texto_aviso_vermelho = None
    if usuario_criado is None:
        texto_aviso_verde = ''
    else:
        texto_aviso_verde = 'Usuário criado com sucesso.'

    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('educacao:index'))

    else:
        if request.method == 'POST':
            form = forms.FormularioEntrar(request.POST)
            if form.is_valid():
                prontuario = form.cleaned_data['prontuario'].lower()
                senha = form.cleaned_data['senha']

                usuario_django = authenticate(username=prontuario, password=senha)
                if usuario_django is not None:
                    # usuario = Usuario.objects.get(prontuario=prontuario)
                    if usuario_django.is_active:
                        login(request, usuario_django)

                        usuario = Usuario.objects.get(usuario_django=usuario_django)
                        usuario.data_ultimo_acesso = datetime.datetime.now()
                        usuario.save()

                        return HttpResponseRedirect(reverse('educacao:index'))
                    else:
                        texto_aviso_vermelho = 'Usuário inativo.'
                else:
                    texto_aviso_vermelho = 'Nome de usuário ou senha está incorreto.'

        contexto = {
            'formulario_entrar': form,
            'texto_aviso_vermelho': texto_aviso_vermelho,
            'texto_aviso_verde': texto_aviso_verde,
        }
        return render(request, 'usuarios/pagina_entrar.html', contexto)


def pagina_criar_conta(request):
    formulario = forms.FormularioCriarConta()
    texto_aviso_verde = None

    if request.method == 'POST':
        formulario = forms.FormularioCriarConta(request.POST)
        if formulario.is_valid():
            primeiro_nome = formulario.cleaned_data['primeiro_nome']
            sobrenome = formulario.cleaned_data['sobrenome']
            prontuario = formulario.cleaned_data['prontuario'].lower()
            email = formulario.cleaned_data['email']
            senha = formulario.cleaned_data['senha']

            usuario_django = User()
            usuario_django.username = prontuario
            usuario_django.first_name = primeiro_nome
            usuario_django.last_name = sobrenome
            usuario_django.email = email
            usuario_django.set_password(senha)
            usuario_django.is_active = True
            usuario_django.is_staff = False
            usuario_django.is_superuser = False
            usuario_django.save()

            novo_usuario = Usuario(
                prontuario=prontuario,
                usuario_django=usuario_django,
                tipo=Usuario.TIPO_ALUNO
            )
            novo_usuario.save()

            return redirect('pagina_entrar_usuario_criado')

    contexto = {
        'formulario': formulario,
        'texto_aviso_verde': texto_aviso_verde,
    }

    if texto_aviso_verde is None:
        return render(request, 'usuarios/pagina_criar_conta.html', contexto)


@login_required
def pagina_sair(request):
    logout(request)
    return redirect('pagina_entrar')

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth.decorators import login_required
from django.forms import TimeInput
from django.http import JsonResponse
from django.views import View
from django.views.generic.base import TemplateView
from django.db.models import Count, Sum, Avg, StdDev
from django.core.serializers import serialize
from django.conf import settings

import datetime
import pytz

from usuarios.views import SentinelaViewBasica
from .forms import *
from .models import *


class LabRemotoViewBasica(SentinelaViewBasica):
    def __init__(self, **kwargs):
        super().__init__(**kwargs)

        self.texto_aviso_verde = None
        self.texto_aviso_vermelho = None

        # self.usuario = None
        # self.instituicao = None
        self.contexto = dict()


class Index(LabRemotoViewBasica):
    def get(self, request, *args, **kwargs):

        kits = KitDidatico.objects.all()
        self.contexto['kits_didaticos'] = kits

        ensaios = Ensaio.objects.all()
        self.contexto['ensaios'] = ensaios

        return render(request, 'lab_remoto/index.html', self.contexto)

    def post(self, request, *args, **kwargs):
        return render(request, 'lab_remoto/index.html', self.contexto)


class CriarKit(LabRemotoViewBasica):
    def get(self, request, *args, **kwargs):

        formulario = FormCriaKit()
        self.contexto['formulario'] = formulario

        return render(request, 'lab_remoto/criar_kit.html', self.contexto)

    def post(self, request, *args, **kwargs):
        form = FormCriaKit(request.POST)

        if form.is_valid():
            form.save()
            self.contexto['form'] = form
            self.contexto['mostrar_barra_aviso_verde'] = True
            self.contexto['criado_com_sucesso'] = True

            # Cria um parâmetro (vazio)
            parametro = ParametroDoKit(
                kit=form.instance,
                nome='',
                sigla='',
                valor=0,
            )
            parametro.save()

        return redirect('lab_remoto:index')


class EditarKit(LabRemotoViewBasica):
    def get(self, request, id_kit, *args, **kwargs):
        self.kit = KitDidatico.objects.get(id=id_kit)

        # Formulário das configurações do kit
        formulario = FormEditarKit(instance=self.kit)
        self.contexto['formulario_confs_kit'] = formulario

        # Formulário para editar os parâmetros do kit
        parametros = ParametroDoKit.objects.filter(kit=self.kit)
        formset = FormsetParametroDoKit(queryset=parametros)
        self.contexto['formset'] = formset
        self.contexto['parametros'] = parametros

        return render(request, 'lab_remoto/editar_kit.html', self.contexto)

    def post(self, request, id_kit, *args, **kwargs):
        self.kit = KitDidatico.objects.get(id=id_kit)
        formulario = FormEditarKit(
            request.POST,
            request.FILES,
            instance=self.kit,
        )
        self.contexto['formulario_confs_kit'] = formulario

        if formulario.is_valid():
            formulario.save()
            self.contexto['mostrar_barra_aviso_verde'] = True
            self.contexto['alteracoes_sucesso'] = True
        else:
            self.contexto['mostrar_barra_aviso_vermelha'] = True
            self.contexto['informacoes_invalidas'] = True

        # Formulário para editar os parâmetros a serem substituídos
        parametros = ParametroDoKit.objects.filter(kit=self.kit)
        self.contexto['parametros'] = parametros
        formset = FormsetParametroDoKit(
            request.POST,
            request.FILES,
            queryset=parametros,
        )

        for formulario in formset:
            if formulario.is_valid():
                formulario.save()

        if 'salvar_e_criar_novo_parametro' in request.POST:
            parametro = ParametroDoKit(
                nome='',
                sigla='',
                kit=self.kit,
                valor=0,
                unidade='',
            )
            parametro.save()

            # Recarrega o formulário
            formset = FormsetParametroDoKit(queryset=parametros)

        self.contexto['formset'] = formset

        return render(request, 'lab_remoto/editar_kit.html', self.contexto)


class RemoverKit(LabRemotoViewBasica):
    def get(self, request, id_kit, *args, **kwargs):
        kit = KitDidatico.objects.get(id=id_kit)
        kit.delete()
        return redirect('lab_remoto:index')

    def post(self, request, id_kit, *args, **kwargs):
        return render(request, 'lab_remoto/remover_kit.html', self.contexto)


class CriarEnsaio(LabRemotoViewBasica):
    def get(self, request, *args, **kwargs):
        formulario = FormCriarEnsaio()
        self.contexto['formulario'] = formulario
        return render(request, 'lab_remoto/criar_ensaio.html', self.contexto)

    def post(self, request, *args, **kwargs):
        form = FormCriarEnsaio(request.POST)

        if form.is_valid():
            novo_ensaio = form.save(commit=False)
            novo_ensaio.professor = self.usuario
            novo_ensaio.save()

            self.contexto['form'] = form
            self.contexto['mostrar_barra_aviso_verde'] = True
            self.contexto['criado_com_sucesso'] = True

            # Cria os parâmetros:
            # - período de amostragem (Ts) e
            # - duração do experimento (tend)
            parametro_Ts = ParametroParaSerInformado(
                nome='Período de amostragem (segundos)',
                ensaio=form.instance,
                sigla='Ts',
                valor_minimo=0,
                valor_maximo=30,
                pode_ser_removido=False,
            )
            parametro_Ts.save()

            parametro_tend = ParametroParaSerInformado(
                nome='Duração do experimento (segundos)',
                ensaio=form.instance,
                sigla='tend',
                valor_minimo=0,
                valor_maximo=30,
                pode_ser_removido=False,
            )
            parametro_tend.save()

        return redirect('lab_remoto:index')


class RemoverEnsaio(LabRemotoViewBasica):
    def get(self, request, id_ensaio, *args, **kwargs):
        ensaio = Ensaio.objects.get(id=id_ensaio)

        parametros = ParametroParaSerInformado.objects.filter(ensaio=ensaio)
        parametros.delete()
        ensaio.delete()

        return redirect('lab_remoto:index')


class EditarEnsaio(LabRemotoViewBasica):
    def comum(self, id_ensaio):
        self.ensaio = Ensaio.objects.get(id=id_ensaio)
        self.contexto['ensaio'] = self.ensaio

        self.parametros_kit = ParametroDoKit.objects.filter(kit=self.ensaio.kit)
        self.contexto['parametros_kit'] = self.parametros_kit

    def get(self, request, id_ensaio, *args, **kwargs):
        self.comum(id_ensaio)

        # Formulário para editar o ensaio
        formulario = FormEditarEnsaio(instance=self.ensaio)
        self.contexto['formulario'] = formulario

        # Formulário para editar os parâmetros a serem substituídos
        parametros = ParametroParaSerInformado.objects.filter(ensaio=self.ensaio)
        formset = FormsetAdicionarParametroParaSerInformado(queryset=parametros)
        self.contexto['formset'] = formset
        self.contexto['parametros'] = parametros

        return render(request, 'lab_remoto/editar_ensaio.html', self.contexto)

    def post(self, request, id_ensaio, *args, **kwargs):
        self.comum(id_ensaio)

        # Formulário das configurações do ensaio
        formulario = FormEditarEnsaio(
            request.POST,
            request.FILES,
            instance=self.ensaio,
        )
        self.contexto['formulario'] = formulario

        if formulario.is_valid():
            formulario.save()
            self.contexto['mostrar_barra_aviso_verde'] = True
            self.contexto['alteracoes_sucesso'] = True
        else:
            self.contexto['mostrar_barra_aviso_vermelha'] = True
            self.contexto['informacoes_invalidas'] = True

        # Formulário para editar os parâmetros a serem substituídos
        parametros = ParametroParaSerInformado.objects.filter(ensaio=self.ensaio)
        self.contexto['parametros'] = parametros
        formset = FormsetAdicionarParametroParaSerInformado(
            request.POST,
            request.FILES,
            queryset=parametros,
        )

        for formulario in formset:
            if formulario.is_valid():
                formulario.save()

        if 'salvar_e_criar_novo_parametro' in request.POST:
            parametro = ParametroParaSerInformado(
                nome='',
                sigla='',
                ensaio=self.ensaio,
                valor_minimo=0,
                valor_maximo=0,
            )
            parametro.save()

            # Recarrega o formulário
            formset = FormsetAdicionarParametroParaSerInformado(queryset=parametros)

        self.contexto['formset'] = formset
        # caso o usuário tenha acabado de alaterar o kit é necessário recarregar os parâmetors do kit
        self.parametros_kit = ParametroDoKit.objects.filter(kit=self.ensaio.kit)
        self.contexto['parametros_kit'] = self.parametros_kit

        return render(request, 'lab_remoto/editar_ensaio.html', self.contexto)


class RemoverParametro(LabRemotoViewBasica):
    def get(self, request, id_parametro, *args, **kwargs):
        parametro = ParametroParaSerInformado.objects.get(
            id=id_parametro
        )
        id_ensaio = parametro.ensaio.id
        parametro.delete()

        # TODO adicionar um teste mostrando se há parâmetros informados pelos estudantes

        return redirect('lab_remoto:editar_ensaio', id_ensaio)


class ExecutarEnsaio(LabRemotoViewBasica):
    def get(self, request, id_ensaio, *args, **kwargs):
        ensaio = Ensaio.objects.get(id=id_ensaio)
        kit = ensaio.kit
        codigo = ensaio.codigo_executavel

        parametros_kit = ParametroDoKit.objects.filter(
            kit=ensaio.kit,
        )

        parametros_necessarios = ParametroParaSerInformado.objects.filter(
            ensaio=ensaio,
        )

        parametros_informados = ParametroInformado.objects.filter(
            ensaio=ensaio,
            usuario=self.usuario,
        )

        if parametros_informados.count() == parametros_necessarios.count():
            # Substituição dos parâmetros
            for parametro in parametros_informados:
                sigla = parametro.parametro.sigla
                valor_informado = parametro.valor_informado
                codigo = codigo.replace('${%s}' % sigla, str(valor_informado))

            for parametro in parametros_kit:
                sigla = parametro.sigla
                valor = parametro.valor
                codigo = codigo.replace('${%s}' % sigla, str(valor))

            # Validação
            # validar oque?

            # Registra a execução do ensaio
            execucao = ExecucaoEnsaio(
                usuario=self.usuario,
                kit=ensaio.kit,
                ensaio=ensaio,
                data_inicio=datetime.datetime.utcnow().replace(tzinfo=pytz.utc),
            )
            execucao.save()

            # O kit agora está ocupado
            ensaio.kit.ocupado = True
            ensaio.kit.save()

            # TODO
            # Gera o código do experimento (preambulo, tipo arduino, setup e loop)
            # Não esquecer de salvar os dados no local correto


            # Roda o ensaio
            now = datetime.datetime.now().strftime("%d-%m-%Y_%H-%M-%S.mat")
            nome_arquivo = '%s_%s' % (self.usuario.prontuario, now)
            nome_arquivo_completo = os.path.join(settings.MEDIA_ROOT, 'upload', 'dados_ensaios', nome_arquivo)
            codigo_ensaio = '\n'.join([
                codigo_preambulo % (kit.porta_serial,),
                codigo,
                codigo_finalizacao % (nome_arquivo_completo,)]
            )
            exec(codigo_ensaio)

            # Registra o horário de término do experimento e o arquivo matlab
            execucao.data_termino=datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
            execucao.arquivo = '/'.join(['upload', 'dados_ensaios', nome_arquivo])
            execucao.save()

            # O kit agora está livre
            ensaio.kit.ocupado = False
            ensaio.kit.save()

        return redirect('lab_remoto:editar_ensaio', id_ensaio)

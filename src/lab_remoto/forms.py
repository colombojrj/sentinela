from django import forms
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import User
from django.forms import formset_factory, modelformset_factory
from crispy_forms.helper import FormHelper

from .models import *
from usuarios.models import Usuario


class FormCriaKit(forms.ModelForm):
    class Meta:
        model = KitDidatico
        fields = (
            'nome',
            'descricao',
            'numero',
            'numero_ensaios_para_manutencao',
        )


class FormEditarKit(forms.ModelForm):
    class Meta:
        model = KitDidatico
        exclude = (
            'data_criacao',
            'ocupado',
            'numero_ensaios_realizados',
        )


FormsetParametroDoKit = modelformset_factory(
    ParametroDoKit,
    exclude=(
        'kit',
    ),
    extra=0,
)


class FormCriarEnsaio(forms.ModelForm):
    class Meta:
        model = Ensaio
        fields = (
            'nome',
            'kit',
            'codigo_executavel',
        )


class FormEditarEnsaio(forms.ModelForm):
    class Meta:
        model = Ensaio
        fields = (
            'nome',
            'kit',
            'codigo_executavel',
            'codigo_grafico',
            'agendavel',
            'executavel',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper(self)
        self.helper.form_show_labels = False


FormsetAdicionarParametroParaSerInformado = modelformset_factory(
    ParametroParaSerInformado,
    exclude=(
        'ensaio',
        'pode_ser_removido',
    ),
    extra=0,
)


class FormEditarTagExecucao(forms.ModelForm):
    class Meta:
        model = ExecucaoEnsaio
        fields = (
            'tag',
            'comentario',
        )

        labels = {
            'tag': 'Tag',
            'comentario': 'Comentário',
        }

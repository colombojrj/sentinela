from django.apps import AppConfig


class LabRemotoConfig(AppConfig):
    name = 'lab_remoto'

from django.urls import path
from django.contrib.auth.decorators import login_required, permission_required

from . import views

app_name = 'lab_remoto'

urlpatterns = [
    path(
        '',
        login_required(views.Index.as_view()),
        name='index'
    ),

    path(
        'criar/kit/',
        login_required(views.CriarKit.as_view()),
        name='criar_kit'
    ),

    path(
        'editar/kit/<int:id_kit>/',
        login_required(views.EditarKit.as_view()),
        name='editar_kit'
    ),

    path(
        'remover/kit/<int:id_kit>/',
        login_required(views.RemoverKit.as_view()),
        name='remover_kit'
    ),

    path(
        'criar/ensaio/',
        login_required(views.CriarEnsaio.as_view()),
        name='criar_ensaio'
    ),

    path(
        'remover/ensaio/<int:id_ensaio>/',
        login_required(views.RemoverEnsaio.as_view()),
        name='remover_ensaio'
    ),

    path(
        'editar/ensaio/<int:id_ensaio>/',
        login_required(views.EditarEnsaio.as_view()),
        name='editar_ensaio'
    ),

    path(
        'remover/parametro/<int:id_parametro>/',
        login_required(views.RemoverParametro.as_view()),
        name='remover_parametro'
    ),

    path(
        'editar/executar/ensaio/<int:id_ensaio>/',
        login_required(views.ExecutarEnsaio.as_view()),
        name='executar_ensaio'
    ),
]



